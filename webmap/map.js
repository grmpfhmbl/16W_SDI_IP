/*	**************************************************************  */
/*	**************************************************************  */
/*	 SDI: Visualization Real Time Data - Winter Term 2016 			*/
/*	 Assignment 2 part 2 - Customized Web Map						*/
/*	 Authors: 	TASHBAEVA Saadat									*/
/*	 		 	REICHEL Steffen 									*/
/*				ABDIMAZHITOVA Kumushkan								*/
/*	 		 	Pleschberger Christian 								*/
/*	 Date: 26.01.2017									 			*/
/*	**************************************************************  */
/*	JavaScript-file used / included in								*/
/*		 					webmap.html								*/
/*	defines actual web content										*/
/*		OSM map, including GeoJSON contents and measurement tool    */
/* 	For additional information see named html-file					*/
/*	**************************************************************  */
/*	**************************************************************  */

var map; // initiate map-object
var measureControls;	// initiate measurement controls object

function init(){
	
	var map = new ol.Map({
			target: document.getElementById('map'),
			layers: [
				new ol.layer.Tile({
					source: new ol.source.OSM(),
					opacity: 1
				})
			],
			controls: [ // additional functionalities (only few are included to not distract the user
				new ol.control.ScaleLine,
				new ol.control.Zoom],
			view: new ol.View({
				// Zoom to Salzburg center
				center: ol.proj.transform([13.05, 47.80], 'EPSG:4326', 'EPSG:3857'),
				zoom: 12
			})
		});
		
		var defStyBoundary = new ol.style.Style({
		stroke: new ol.style.Stroke(({
			color: 'red',
			width: 3,
			lineDash: [10,10],
			fill: 'transparent'
		}))
	});
	var boundary = new ol.layer.Vector({
		title: 'Boundary Salzburg',
		source: new ol.source.Vector({
			projection : 'EPSG:4326',
			url: 'Salzburg.json',
			format: new ol.format.GeoJSON()	
		}),
		style: defStyBoundary
	});
	map.addLayer(boundary);
	


} // init()





