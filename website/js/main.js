/**
 * OfferingType contains an offering with some metadata
 * @typedef{Object} OfferingType
 * @property {string} name - a human readable name
 * @property {string} identifier - the identifier that is used in the SOS
 */

/**
 * my global namespace
 */
var ipsdi = (function (ns) {
//prevent global namespace from pollution - yeah require() would be nicer ;-)

    /**
     * list of all know offerings
     * @type{OfferingType[]}
     */
    ns.offerings = [
        {
            "name": "Open Weather Map",
            "identifier": "http://ip-sdi/offering/openweathermap/latlon",
            "properties": [
                "http://ip-sdi/phenomenon/temperature",
                "http://ip-sdi/phenomenon/humidity",
                "http://ip-sdi/phenomenon/grndlevelpressure",
                "http://ip-sdi/phenomenon/sealevelpressure"
            ],
            "popup_widget": {
                "name": "compass",
                "property": "http://ip-sdi/phenomenon/winddirection",
                "refresh_interval": 60
            },
            "panels_visible":["windrose-panel", "timechart-panel", "airpressure-panel"],
            "onclick": function (event) {
                ns.initTimechart(event.layer.feature,
                    ["http://ip-sdi/phenomenon/temperature",
                        "http://ip-sdi/phenomenon/humidity"],
                    "http://ip-sdi/offering/openweathermap/latlon",
                    "timechart",
                "temperature / humidity");
                ns.initWindrose(event.layer.feature,
                    ["http://ip-sdi/phenomenon/windspeed",
                        "http://ip-sdi/phenomenon/winddirection"],
                    "http://ip-sdi/offering/openweathermap/latlon");
                ns.initTimechart(event.layer.feature,
                    ["http://ip-sdi/phenomenon/grndlevelpressure",
                        "http://ip-sdi/phenomenon/sealevelpressure"],
                    "http://ip-sdi/offering/openweathermap/latlon",
                    "airpressure",
                    "airpressure");
            }
        },
        {
            "name": "HYDRIS waterlevel",
            "identifier": "http://ip-sdi/offering/hydris",
            "properties": ["http://ip-sdi/phenomenon/waterlevel"],
            "panels_visible": ["timechart-panel"],
            "onclick": function (event) {
                ns.initTimechart(event.layer.feature,
                    ["http://ip-sdi/phenomenon/waterlevel"],
                    "http://ip-sdi/offering/hydris",
                "timechart",
                "waterlevel");
            }
        }
    ];

    /**
     * on selecting a new offering, reinitialize the map
     * and the rest of the UI
     * @param offeringIndex
     */
    ns.selectOffering = function (offeringIndex) {
        ns.initMapWidget(ns.offerings[offeringIndex]);
        $("#offerings-dropdown > li").removeClass("active");
        $("#offerings-dropdown > li:nth-child(" + (offeringIndex + 1) + ")").addClass("active");

        $("#main-accordion > .panel").addClass("hidden");
        ns.offerings[offeringIndex].panels_visible.forEach(function(item) {
            $("#"+item).removeClass("hidden");
        });
    }

    /**
     * initializes the whole application
     */
    ns.init = function () {
        ns.selectOffering(0);
    }

    /**
     * initializes the central map widget
     * @param offering
     */
    ns.initMapWidget = function (offering) {
        SensorWidget('map', {
            "service": "http://104.199.84.35/sos/service/json",
            "offering": offering.identifier,
            "features": [],
            "properties": offering.properties,
            "footnote": "SOS data from http://104.199.84.35/sos/",
            "popup_widget": offering.popup_widget,
            "onclick": offering.onclick
        }, document.getElementById('map-container'));

        ns.resizeMap();
    };

    ns.initTimechart = function (feature, phenomenons, offering, container, caption) {
        $("#"+container+"-container").css("height", 450 + "px");
        $("#"+container+"-heading > h4 > a").text(caption);
        SensorWidget('timechart', {
            "service": "http://104.199.84.35/sos/service/json",
            "offering": offering,
            "title": feature.properties.name,
            "features": [
                feature.id
            ],
            "properties": phenomenons
            ,
            "time_start": moment().subtract(1, 'week').format(),
            "time_end": moment().format(),
            "footnote": "queried: " + moment().subtract(1, 'week').format("YYYY-MM-DD HH:mm") + " to " + moment().format("YYYY-MM-DD HH:mm")
        }, document.getElementById(container+"-container"));
    };

    ns.initWindrose = function (feature, phenomenons, offering) {
        //$("#windrose-container").css("height", 300 + "px");
        SensorWidget('windrose', {
            "service": "http://104.199.84.35/sos/service/json",
            "offering": offering,
            "title": feature.properties.name,
            "feature": feature.id,
            "properties": phenomenons
            ,
            "time_start": moment().subtract(1, 'week').format(),
            "time_end": moment().format(),
            "refresh_interval": 600,
            "footnote": "queried: " + moment().subtract(1, 'week').format("YYYY-MM-DD HH:mm") + " to " + moment().format("YYYY-MM-DD HH:mm")
        }, document.getElementById('windrose-container'));
    };

    /**
     * little hack: without this, the map window will not show.
     */
    ns.resizeMap = function () {
        var heights = window.innerHeight;
        var widths = window.innerWidth;
        $("#map-container").css("height", (heights - 50) + "px");
        //document.getElementById("map-container").style.height = (heights - 50) + "px";
    }

    /**
     * @see ns.resizeMap
     */
    window.onresize = function () {
        ns.resizeMap();
    };

    return ns;
})(window.ipsdi || {});