define("widget/panel", ["i18n", "sos-data-access", "locale-date", "widget-common"], function (e, t, n, r) {
    "use strict";
    var a = ['<div class="panel widget">', "<h2></h2>", "<h3>", e.t("Loading..."), "</h3>", '<dl class="dl-horizontal"></dl>', '<div><span class="footnote"></span></div>', "</div>"].join("");
    return {
        inputs: r.inputs.concat(["feature", "properties", "refresh_interval", "title"]),
        optional_inputs: r.optional_inputs,
        preferredSizes: [{w: 400, h: 400}],
        init: function (i, o, d) {
            function l(t) {
                if (!t.length)return void(s.innerHTML = e.t("(no data)"));
                var r = new Date(Math.max.apply(Math, t.map(function (e) {
                    return e.time
                })));
                t.sort(function (e, t) {
                    return e.property.localeCompare(t.property)
                }), s.innerHTML = n.display(r);
                var a = "";
                for (var i in t) {
                    var o = t[i];
                    a += "<dt>" + o.property + "</dt>", a += o.time.getTime() == r.getTime() ? "<dd>" + o.value + " " + o.uom + "</dd>" : "<dd class='outdated'>" + o.value + " " + o.uom + "* <span>*(" + n.display(o.time) + ")</span></dd>"
                }
                p.innerHTML = a
            }

            o.innerHTML = a, o.querySelector("h2").innerHTML = i.title;
            var s = o.querySelector("h3"), p = o.querySelector("dl");
            r.init(i, o);
            var u = t(i, l, d), c = setInterval(u.read, 1e3 * i.refresh_interval);
            return u.read(), {
                destroy: function () {
                    clearInterval(c)
                }
            }
        }
    }
});