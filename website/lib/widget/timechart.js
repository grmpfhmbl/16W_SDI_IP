/* Javascript plotting library for jQuery, version 0.8.3.

 Copyright (c) 2007-2014 IOLA and Ole Laursen.
 Licensed under the MIT license.

 */

/* Plugin for jQuery for working with colors.
 *
 * Version 1.1.
 *
 * Inspiration from jQuery color animation plugin by John Resig.
 *
 * Released under the MIT license by Ole Laursen, October 2009.
 *
 * Examples:
 *
 *   $.color.parse("#fff").scale('rgb', 0.25).add('a', -0.5).toString()
 *   var c = $.color.extract($("#mydiv"), 'background-color');
 *   console.log(c.r, c.g, c.b, c.a);
 *   $.color.make(100, 50, 25, 0.4).toString() // returns "rgba(100,50,25,0.4)"
 *
 * Note that .scale() and .add() return the same modified object
 * instead of making a new one.
 *
 * V. 1.1: Fix error handling so e.g. parsing an empty string does
 * produce a color rather than just crashing.
 */

/* Flot plugin for automatically redrawing plots as the placeholder resizes.

 Copyright (c) 2007-2014 IOLA and Ole Laursen.
 Licensed under the MIT license.

 It works by listening for changes on the placeholder div (through the jQuery
 resize event plugin) - if the size changes, it will redraw the plot.

 There are no options. If you need to disable the plugin for some plots, you
 can just fix the size of their placeholders.

 */

/* Inline dependency:
 * jQuery resize event - v1.1 - 3/14/2010
 * http://benalman.com/projects/jquery-resize-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

/* Pretty handling of time axes.

 Copyright (c) 2007-2014 IOLA and Ole Laursen.
 Licensed under the MIT license.

 Set axis.mode to "time" to enable. See the section "Time series data" in
 API.txt for details.

 */

/* Flot plugin for adding the ability to pan and zoom the plot.

 Copyright (c) 2007-2014 IOLA and Ole Laursen.
 Licensed under the MIT license.

 The default behaviour is double click and scrollwheel up/down to zoom in, drag
 to pan. The plugin defines plot.zoom({ center }), plot.zoomOut() and
 plot.pan( offset ) so you easily can add custom controls. It also fires
 "plotpan" and "plotzoom" events, useful for synchronizing plots.

 The plugin supports these options:

 zoom: {
 interactive: false
 trigger: "dblclick" // or "click" for single click
 amount: 1.5         // 2 = 200% (zoom in), 0.5 = 50% (zoom out)
 }

 pan: {
 interactive: false
 cursor: "move"      // CSS mouse cursor value used when dragging, e.g. "pointer"
 frameRate: 20
 }

 xaxis, yaxis, x2axis, y2axis: {
 zoomRange: null  // or [ number, number ] (min range, max range) or false
 panRange: null   // or [ number, number ] (min, max) or false
 }

 "interactive" enables the built-in drag/click behaviour. If you enable
 interactive for pan, then you'll have a basic plot that supports moving
 around; the same for zoom.

 "amount" specifies the default amount to zoom in (so 1.5 = 150%) relative to
 the current viewport.

 "cursor" is a standard CSS mouse cursor string used for visual feedback to the
 user when dragging.

 "frameRate" specifies the maximum number of times per second the plot will
 update itself while the user is panning around on it (set to null to disable
 intermediate pans, the plot will then not update until the mouse button is
 released).

 "zoomRange" is the interval in which zooming can happen, e.g. with zoomRange:
 [1, 100] the zoom will never scale the axis so that the difference between min
 and max is smaller than 1 or larger than 100. You can set either end to null
 to ignore, e.g. [1, null]. If you set zoomRange to false, zooming on that axis
 will be disabled.

 "panRange" confines the panning to stay within a range, e.g. with panRange:
 [-10, 20] panning stops at -10 in one end and at 20 in the other. Either can
 be null, e.g. [-10, null]. If you set panRange to false, panning on that axis
 will be disabled.

 Example API usage:

 plot = $.plot(...);

 // zoom default amount in on the pixel ( 10, 20 )
 plot.zoom({ center: { left: 10, top: 20 } });

 // zoom out again
 plot.zoomOut({ center: { left: 10, top: 20 } });

 // zoom 200% in on the pixel (10, 20)
 plot.zoom({ amount: 2, center: { left: 10, top: 20 } });

 // pan 100 pixels to the left and 20 down
 plot.pan({ left: -100, top: 20 })

 Here, "center" specifies where the center of the zooming should happen. Note
 that this is defined in pixel space, not the space of the data points (you can
 use the p2c helpers on the axes in Flot to help you convert between these).

 "amount" is the amount to zoom the viewport relative to the current range, so
 1 is 100% (i.e. no change), 1.5 is 150% (zoom in), 0.7 is 70% (zoom out). You
 can set the default in the options.

 */

/*
 jquery.event.drag.js ~ v1.5 ~ Copyright (c) 2008, Three Dub Media (http://threedubmedia.com)
 Licensed under the MIT License ~ http://threedubmedia.googlecode.com/files/MIT-LICENSE.txt
 */

/* jquery.mousewheel.min.js
 * Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 *
 * Requires: 1.2.2+
 */

!function (t) {
    define("flot", ["jquery"], function () {
        return function () {
            require(["jquery"], function (t) {
                !function (t) {
                    t.color = {}, t.color.make = function (e, i, o, n) {
                        var a = {};
                        return a.r = e || 0, a.g = i || 0, a.b = o || 0, a.a = null != n ? n : 1, a.add = function (t, e) {
                            for (var i = 0; i < t.length; ++i)a[t.charAt(i)] += e;
                            return a.normalize()
                        }, a.scale = function (t, e) {
                            for (var i = 0; i < t.length; ++i)a[t.charAt(i)] *= e;
                            return a.normalize()
                        }, a.toString = function () {
                            return a.a >= 1 ? "rgb(" + [a.r, a.g, a.b].join(",") + ")" : "rgba(" + [a.r, a.g, a.b, a.a].join(",") + ")"
                        }, a.normalize = function () {
                            function t(t, e, i) {
                                return t > e ? t : e > i ? i : e
                            }

                            return a.r = t(0, parseInt(a.r), 255), a.g = t(0, parseInt(a.g), 255), a.b = t(0, parseInt(a.b), 255), a.a = t(0, a.a, 1), a
                        }, a.clone = function () {
                            return t.color.make(a.r, a.b, a.g, a.a)
                        }, a.normalize()
                    }, t.color.extract = function (e, i) {
                        var o;
                        do {
                            if (o = e.css(i).toLowerCase(), "" != o && "transparent" != o)break;
                            e = e.parent()
                        } while (e.length && !t.nodeName(e.get(0), "body"));
                        return "rgba(0, 0, 0, 0)" == o && (o = "transparent"), t.color.parse(o)
                    }, t.color.parse = function (i) {
                        var o, n = t.color.make;
                        if (o = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(i))return n(parseInt(o[1], 10), parseInt(o[2], 10), parseInt(o[3], 10));
                        if (o = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]+(?:\.[0-9]+)?)\s*\)/.exec(i))return n(parseInt(o[1], 10), parseInt(o[2], 10), parseInt(o[3], 10), parseFloat(o[4]));
                        if (o = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(i))return n(2.55 * parseFloat(o[1]), 2.55 * parseFloat(o[2]), 2.55 * parseFloat(o[3]));
                        if (o = /rgba\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\s*\)/.exec(i))return n(2.55 * parseFloat(o[1]), 2.55 * parseFloat(o[2]), 2.55 * parseFloat(o[3]), parseFloat(o[4]));
                        if (o = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(i))return n(parseInt(o[1], 16), parseInt(o[2], 16), parseInt(o[3], 16));
                        if (o = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(i))return n(parseInt(o[1] + o[1], 16), parseInt(o[2] + o[2], 16), parseInt(o[3] + o[3], 16));
                        var a = t.trim(i).toLowerCase();
                        return "transparent" == a ? n(255, 255, 255, 0) : (o = e[a] || [0, 0, 0], n(o[0], o[1], o[2]))
                    };
                    var e = {
                        aqua: [0, 255, 255],
                        azure: [240, 255, 255],
                        beige: [245, 245, 220],
                        black: [0, 0, 0],
                        blue: [0, 0, 255],
                        brown: [165, 42, 42],
                        cyan: [0, 255, 255],
                        darkblue: [0, 0, 139],
                        darkcyan: [0, 139, 139],
                        darkgrey: [169, 169, 169],
                        darkgreen: [0, 100, 0],
                        darkkhaki: [189, 183, 107],
                        darkmagenta: [139, 0, 139],
                        darkolivegreen: [85, 107, 47],
                        darkorange: [255, 140, 0],
                        darkorchid: [153, 50, 204],
                        darkred: [139, 0, 0],
                        darksalmon: [233, 150, 122],
                        darkviolet: [148, 0, 211],
                        fuchsia: [255, 0, 255],
                        gold: [255, 215, 0],
                        green: [0, 128, 0],
                        indigo: [75, 0, 130],
                        khaki: [240, 230, 140],
                        lightblue: [173, 216, 230],
                        lightcyan: [224, 255, 255],
                        lightgreen: [144, 238, 144],
                        lightgrey: [211, 211, 211],
                        lightpink: [255, 182, 193],
                        lightyellow: [255, 255, 224],
                        lime: [0, 255, 0],
                        magenta: [255, 0, 255],
                        maroon: [128, 0, 0],
                        navy: [0, 0, 128],
                        olive: [128, 128, 0],
                        orange: [255, 165, 0],
                        pink: [255, 192, 203],
                        purple: [128, 0, 128],
                        violet: [128, 0, 128],
                        red: [255, 0, 0],
                        silver: [192, 192, 192],
                        white: [255, 255, 255],
                        yellow: [255, 255, 0]
                    }
                }(t), function (t) {
                    function e(e, i) {
                        var o = i.children("." + e)[0];
                        if (null == o && (o = document.createElement("canvas"), o.className = e, t(o).css({
                                direction: "ltr",
                                position: "absolute",
                                left: 0,
                                top: 0
                            }).appendTo(i), !o.getContext)) {
                            if (!window.G_vmlCanvasManager)throw new Error("Canvas is not available. If you're using IE with a fall-back such as Excanvas, then there's either a mistake in your conditional include, or the page has no DOCTYPE and is rendering in Quirks Mode.");
                            o = window.G_vmlCanvasManager.initElement(o)
                        }
                        this.element = o;
                        var n = this.context = o.getContext("2d"), a = window.devicePixelRatio || 1, r = n.webkitBackingStorePixelRatio || n.mozBackingStorePixelRatio || n.msBackingStorePixelRatio || n.oBackingStorePixelRatio || n.backingStorePixelRatio || 1;
                        this.pixelRatio = a / r, this.resize(i.width(), i.height()), this.textContainer = null, this.text = {}, this._textCache = {}
                    }

                    function i(i, n, a, r) {
                        function s(t, e) {
                            e = [ge].concat(e);
                            for (var i = 0; i < t.length; ++i)t[i].apply(this, e)
                        }

                        function l() {
                            for (var i = {Canvas: e}, o = 0; o < r.length; ++o) {
                                var n = r[o];
                                n.init(ge, i), n.options && t.extend(!0, ne, n.options)
                            }
                        }

                        function c(e) {
                            t.extend(!0, ne, e), e && e.colors && (ne.colors = e.colors), null == ne.xaxis.color && (ne.xaxis.color = t.color.parse(ne.grid.color).scale("a", .22).toString()), null == ne.yaxis.color && (ne.yaxis.color = t.color.parse(ne.grid.color).scale("a", .22).toString()), null == ne.xaxis.tickColor && (ne.xaxis.tickColor = ne.grid.tickColor || ne.xaxis.color), null == ne.yaxis.tickColor && (ne.yaxis.tickColor = ne.grid.tickColor || ne.yaxis.color), null == ne.grid.borderColor && (ne.grid.borderColor = ne.grid.color), null == ne.grid.tickColor && (ne.grid.tickColor = t.color.parse(ne.grid.color).scale("a", .22).toString());
                            var o, n, a, r = i.css("font-size"), l = r ? +r.replace("px", "") : 13, c = {
                                style: i.css("font-style"),
                                size: Math.round(.8 * l),
                                variant: i.css("font-variant"),
                                weight: i.css("font-weight"),
                                family: i.css("font-family")
                            };
                            for (a = ne.xaxes.length || 1, o = 0; a > o; ++o)n = ne.xaxes[o], n && !n.tickColor && (n.tickColor = n.color), n = t.extend(!0, {}, ne.xaxis, n), ne.xaxes[o] = n, n.font && (n.font = t.extend({}, c, n.font), n.font.color || (n.font.color = n.color), n.font.lineHeight || (n.font.lineHeight = Math.round(1.15 * n.font.size)));
                            for (a = ne.yaxes.length || 1, o = 0; a > o; ++o)n = ne.yaxes[o], n && !n.tickColor && (n.tickColor = n.color), n = t.extend(!0, {}, ne.yaxis, n), ne.yaxes[o] = n, n.font && (n.font = t.extend({}, c, n.font), n.font.color || (n.font.color = n.color), n.font.lineHeight || (n.font.lineHeight = Math.round(1.15 * n.font.size)));
                            for (ne.xaxis.noTicks && null == ne.xaxis.ticks && (ne.xaxis.ticks = ne.xaxis.noTicks), ne.yaxis.noTicks && null == ne.yaxis.ticks && (ne.yaxis.ticks = ne.yaxis.noTicks), ne.x2axis && (ne.xaxes[1] = t.extend(!0, {}, ne.xaxis, ne.x2axis), ne.xaxes[1].position = "top", null == ne.x2axis.min && (ne.xaxes[1].min = null), null == ne.x2axis.max && (ne.xaxes[1].max = null)), ne.y2axis && (ne.yaxes[1] = t.extend(!0, {}, ne.yaxis, ne.y2axis), ne.yaxes[1].position = "right", null == ne.y2axis.min && (ne.yaxes[1].min = null), null == ne.y2axis.max && (ne.yaxes[1].max = null)), ne.grid.coloredAreas && (ne.grid.markings = ne.grid.coloredAreas), ne.grid.coloredAreasColor && (ne.grid.markingsColor = ne.grid.coloredAreasColor), ne.lines && t.extend(!0, ne.series.lines, ne.lines), ne.points && t.extend(!0, ne.series.points, ne.points), ne.bars && t.extend(!0, ne.series.bars, ne.bars), null != ne.shadowSize && (ne.series.shadowSize = ne.shadowSize), null != ne.highlightColor && (ne.series.highlightColor = ne.highlightColor), o = 0; o < ne.xaxes.length; ++o)g(ue, o + 1).options = ne.xaxes[o];
                            for (o = 0; o < ne.yaxes.length; ++o)g(he, o + 1).options = ne.yaxes[o];
                            for (var u in me)ne.hooks[u] && ne.hooks[u].length && (me[u] = me[u].concat(ne.hooks[u]));
                            s(me.processOptions, [ne])
                        }

                        function u(t) {
                            oe = h(t), x(), v()
                        }

                        function h(e) {
                            for (var i = [], o = 0; o < e.length; ++o) {
                                var n = t.extend(!0, {}, ne.series);
                                null != e[o].data ? (n.data = e[o].data, delete e[o].data, t.extend(!0, n, e[o]), e[o].data = n.data) : n.data = e[o], i.push(n)
                            }
                            return i
                        }

                        function p(t, e) {
                            var i = t[e + "axis"];
                            return "object" == typeof i && (i = i.n), "number" != typeof i && (i = 1), i
                        }

                        function f() {
                            return t.grep(ue.concat(he), function (t) {
                                return t
                            })
                        }

                        function d(t) {
                            var e, i, o = {};
                            for (e = 0; e < ue.length; ++e)i = ue[e], i && i.used && (o["x" + i.n] = i.c2p(t.left));
                            for (e = 0; e < he.length; ++e)i = he[e], i && i.used && (o["y" + i.n] = i.c2p(t.top));
                            return void 0 !== o.x1 && (o.x = o.x1), void 0 !== o.y1 && (o.y = o.y1), o
                        }

                        function m(t) {
                            var e, i, o, n = {};
                            for (e = 0; e < ue.length; ++e)if (i = ue[e], i && i.used && (o = "x" + i.n, null == t[o] && 1 == i.n && (o = "x"), null != t[o])) {
                                n.left = i.p2c(t[o]);
                                break
                            }
                            for (e = 0; e < he.length; ++e)if (i = he[e], i && i.used && (o = "y" + i.n, null == t[o] && 1 == i.n && (o = "y"), null != t[o])) {
                                n.top = i.p2c(t[o]);
                                break
                            }
                            return n
                        }

                        function g(e, i) {
                            return e[i - 1] || (e[i - 1] = {
                                n: i,
                                direction: e == ue ? "x" : "y",
                                options: t.extend(!0, {}, e == ue ? ne.xaxis : ne.yaxis)
                            }), e[i - 1]
                        }

                        function x() {
                            var e, i = oe.length, o = -1;
                            for (e = 0; e < oe.length; ++e) {
                                var n = oe[e].color;
                                null != n && (i--, "number" == typeof n && n > o && (o = n))
                            }
                            o >= i && (i = o + 1);
                            var a, r = [], s = ne.colors, l = s.length, c = 0;
                            for (e = 0; i > e; e++)a = t.color.parse(s[e % l] || "#666"), e % l == 0 && e && (c = c >= 0 ? .5 > c ? -c - .2 : 0 : -c), r[e] = a.scale("rgb", 1 + c);
                            var u, h = 0;
                            for (e = 0; e < oe.length; ++e) {
                                if (u = oe[e], null == u.color ? (u.color = r[h].toString(), ++h) : "number" == typeof u.color && (u.color = r[u.color].toString()), null == u.lines.show) {
                                    var f, d = !0;
                                    for (f in u)if (u[f] && u[f].show) {
                                        d = !1;
                                        break
                                    }
                                    d && (u.lines.show = !0)
                                }
                                null == u.lines.zero && (u.lines.zero = !!u.lines.fill), u.xaxis = g(ue, p(u, "x")), u.yaxis = g(he, p(u, "y"))
                            }
                        }

                        function v() {
                            function e(t, e, i) {
                                e < t.datamin && e != -v && (t.datamin = e), i > t.datamax && i != v && (t.datamax = i)
                            }

                            var i, o, n, a, r, l, c, u, h, p, d, m, g = Number.POSITIVE_INFINITY, x = Number.NEGATIVE_INFINITY, v = Number.MAX_VALUE;
                            for (t.each(f(), function (t, e) {
                                e.datamin = g, e.datamax = x, e.used = !1
                            }), i = 0; i < oe.length; ++i)r = oe[i], r.datapoints = {points: []}, s(me.processRawData, [r, r.data, r.datapoints]);
                            for (i = 0; i < oe.length; ++i) {
                                if (r = oe[i], d = r.data, m = r.datapoints.format, !m) {
                                    if (m = [], m.push({x: !0, number: !0, required: !0}), m.push({
                                            y: !0,
                                            number: !0,
                                            required: !0
                                        }), r.bars.show || r.lines.show && r.lines.fill) {
                                        var b = !!(r.bars.show && r.bars.zero || r.lines.show && r.lines.zero);
                                        m.push({
                                            y: !0,
                                            number: !0,
                                            required: !1,
                                            defaultValue: 0,
                                            autoscale: b
                                        }), r.bars.horizontal && (delete m[m.length - 1].y, m[m.length - 1].x = !0)
                                    }
                                    r.datapoints.format = m
                                }
                                if (null == r.datapoints.pointsize) {
                                    r.datapoints.pointsize = m.length, c = r.datapoints.pointsize, l = r.datapoints.points;
                                    var y = r.lines.show && r.lines.steps;
                                    for (r.xaxis.used = r.yaxis.used = !0, o = n = 0; o < d.length; ++o, n += c) {
                                        p = d[o];
                                        var k = null == p;
                                        if (!k)for (a = 0; c > a; ++a)u = p[a], h = m[a], h && (h.number && null != u && (u = +u, isNaN(u) ? u = null : 1 / 0 == u ? u = v : u == -1 / 0 && (u = -v)), null == u && (h.required && (k = !0), null != h.defaultValue && (u = h.defaultValue))), l[n + a] = u;
                                        if (k)for (a = 0; c > a; ++a)u = l[n + a], null != u && (h = m[a], h.autoscale !== !1 && (h.x && e(r.xaxis, u, u), h.y && e(r.yaxis, u, u))), l[n + a] = null; else if (y && n > 0 && null != l[n - c] && l[n - c] != l[n] && l[n - c + 1] != l[n + 1]) {
                                            for (a = 0; c > a; ++a)l[n + c + a] = l[n + a];
                                            l[n + 1] = l[n - c + 1], n += c
                                        }
                                    }
                                }
                            }
                            for (i = 0; i < oe.length; ++i)r = oe[i], s(me.processDatapoints, [r, r.datapoints]);
                            for (i = 0; i < oe.length; ++i) {
                                r = oe[i], l = r.datapoints.points, c = r.datapoints.pointsize, m = r.datapoints.format;
                                var w = g, M = g, T = x, z = x;
                                for (o = 0; o < l.length; o += c)if (null != l[o])for (a = 0; c > a; ++a)u = l[o + a], h = m[a], h && h.autoscale !== !1 && u != v && u != -v && (h.x && (w > u && (w = u), u > T && (T = u)), h.y && (M > u && (M = u), u > z && (z = u)));
                                if (r.bars.show) {
                                    var S;
                                    switch (r.bars.align) {
                                        case"left":
                                            S = 0;
                                            break;
                                        case"right":
                                            S = -r.bars.barWidth;
                                            break;
                                        default:
                                            S = -r.bars.barWidth / 2
                                    }
                                    r.bars.horizontal ? (M += S, z += S + r.bars.barWidth) : (w += S, T += S + r.bars.barWidth)
                                }
                                e(r.xaxis, w, T), e(r.yaxis, M, z)
                            }
                            t.each(f(), function (t, e) {
                                e.datamin == g && (e.datamin = null), e.datamax == x && (e.datamax = null)
                            })
                        }

                        function b() {
                            i.css("padding", 0).children().filter(function () {
                                return !t(this).hasClass("flot-overlay") && !t(this).hasClass("flot-base")
                            }).remove(), "static" == i.css("position") && i.css("position", "relative"), ae = new e("flot-base", i), re = new e("flot-overlay", i), le = ae.context, ce = re.context, se = t(re.element).unbind();
                            var o = i.data("plot");
                            o && (o.shutdown(), re.clear()), i.data("plot", ge)
                        }

                        function y() {
                            ne.grid.hoverable && (se.mousemove(_), se.bind("mouseleave", B)), ne.grid.clickable && se.click(V), s(me.bindEvents, [se])
                        }

                        function k() {
                            ve && clearTimeout(ve), se.unbind("mousemove", _), se.unbind("mouseleave", B), se.unbind("click", V), s(me.shutdown, [se])
                        }

                        function w(t) {
                            function e(t) {
                                return t
                            }

                            var i, o, n = t.options.transform || e, a = t.options.inverseTransform;
                            "x" == t.direction ? (i = t.scale = fe / Math.abs(n(t.max) - n(t.min)), o = Math.min(n(t.max), n(t.min))) : (i = t.scale = de / Math.abs(n(t.max) - n(t.min)), i = -i, o = Math.max(n(t.max), n(t.min))), t.p2c = n == e ? function (t) {
                                    return (t - o) * i
                                } : function (t) {
                                    return (n(t) - o) * i
                                }, t.c2p = a ? function (t) {
                                    return a(o + t / i)
                                } : function (t) {
                                    return o + t / i
                                }
                        }

                        function M(t) {
                            for (var e = t.options, i = t.ticks || [], o = e.labelWidth || 0, n = e.labelHeight || 0, a = o || ("x" == t.direction ? Math.floor(ae.width / (i.length || 1)) : null), r = t.direction + "Axis " + t.direction + t.n + "Axis", s = "flot-" + t.direction + "-axis flot-" + t.direction + t.n + "-axis " + r, l = e.font || "flot-tick-label tickLabel", c = 0; c < i.length; ++c) {
                                var u = i[c];
                                if (u.label) {
                                    var h = ae.getTextInfo(s, u.label, l, null, a);
                                    o = Math.max(o, h.width), n = Math.max(n, h.height)
                                }
                            }
                            t.labelWidth = e.labelWidth || o, t.labelHeight = e.labelHeight || n
                        }

                        function T(e) {
                            var i = e.labelWidth, o = e.labelHeight, n = e.options.position, a = "x" === e.direction, r = e.options.tickLength, s = ne.grid.axisMargin, l = ne.grid.labelMargin, c = !0, u = !0, h = !0, p = !1;
                            t.each(a ? ue : he, function (t, i) {
                                i && (i.show || i.reserveSpace) && (i === e ? p = !0 : i.options.position === n && (p ? u = !1 : c = !1), p || (h = !1))
                            }), u && (s = 0), null == r && (r = h ? "full" : 5), isNaN(+r) || (l += +r), a ? (o += l, "bottom" == n ? (pe.bottom += o + s, e.box = {
                                        top: ae.height - pe.bottom,
                                        height: o
                                    }) : (e.box = {
                                        top: pe.top + s,
                                        height: o
                                    }, pe.top += o + s)) : (i += l, "left" == n ? (e.box = {
                                        left: pe.left + s,
                                        width: i
                                    }, pe.left += i + s) : (pe.right += i + s, e.box = {
                                        left: ae.width - pe.right,
                                        width: i
                                    })), e.position = n, e.tickLength = r, e.box.padding = l, e.innermost = c
                        }

                        function z(t) {
                            "x" == t.direction ? (t.box.left = pe.left - t.labelWidth / 2, t.box.width = ae.width - pe.left - pe.right + t.labelWidth) : (t.box.top = pe.top - t.labelHeight / 2, t.box.height = ae.height - pe.bottom - pe.top + t.labelHeight)
                        }

                        function S() {
                            var e, i = ne.grid.minBorderMargin;
                            if (null == i)for (i = 0, e = 0; e < oe.length; ++e)i = Math.max(i, 2 * (oe[e].points.radius + oe[e].points.lineWidth / 2));
                            var o = {left: i, right: i, top: i, bottom: i};
                            t.each(f(), function (t, e) {
                                e.reserveSpace && e.ticks && e.ticks.length && ("x" === e.direction ? (o.left = Math.max(o.left, e.labelWidth / 2), o.right = Math.max(o.right, e.labelWidth / 2)) : (o.bottom = Math.max(o.bottom, e.labelHeight / 2), o.top = Math.max(o.top, e.labelHeight / 2)))
                            }), pe.left = Math.ceil(Math.max(o.left, pe.left)), pe.right = Math.ceil(Math.max(o.right, pe.right)), pe.top = Math.ceil(Math.max(o.top, pe.top)), pe.bottom = Math.ceil(Math.max(o.bottom, pe.bottom))
                        }

                        function C() {
                            var e, i = f(), o = ne.grid.show;
                            for (var n in pe) {
                                var a = ne.grid.margin || 0;
                                pe[n] = "number" == typeof a ? a : a[n] || 0
                            }
                            s(me.processOffset, [pe]);
                            for (var n in pe)pe[n] += "object" == typeof ne.grid.borderWidth ? o ? ne.grid.borderWidth[n] : 0 : o ? ne.grid.borderWidth : 0;
                            if (t.each(i, function (t, e) {
                                    var i = e.options;
                                    e.show = null == i.show ? e.used : i.show, e.reserveSpace = null == i.reserveSpace ? e.show : i.reserveSpace, O(e)
                                }), o) {
                                var r = t.grep(i, function (t) {
                                    return t.show || t.reserveSpace
                                });
                                for (t.each(r, function (t, e) {
                                    F(e), D(e), A(e, e.ticks), M(e)
                                }), e = r.length - 1; e >= 0; --e)T(r[e]);
                                S(), t.each(r, function (t, e) {
                                    z(e)
                                })
                            }
                            fe = ae.width - pe.left - pe.right, de = ae.height - pe.bottom - pe.top, t.each(i, function (t, e) {
                                w(e)
                            }), o && N(), j()
                        }

                        function O(t) {
                            var e = t.options, i = +(null != e.min ? e.min : t.datamin), o = +(null != e.max ? e.max : t.datamax), n = o - i;
                            if (0 == n) {
                                var a = 0 == o ? 1 : .01;
                                null == e.min && (i -= a), (null == e.max || null != e.min) && (o += a)
                            } else {
                                var r = e.autoscaleMargin;
                                null != r && (null == e.min && (i -= n * r, 0 > i && null != t.datamin && t.datamin >= 0 && (i = 0)), null == e.max && (o += n * r, o > 0 && null != t.datamax && t.datamax <= 0 && (o = 0)))
                            }
                            t.min = i, t.max = o
                        }

                        function F(e) {
                            var i, n = e.options;
                            i = "number" == typeof n.ticks && n.ticks > 0 ? n.ticks : .3 * Math.sqrt("x" == e.direction ? ae.width : ae.height);
                            var a = (e.max - e.min) / i, r = -Math.floor(Math.log(a) / Math.LN10), s = n.tickDecimals;
                            null != s && r > s && (r = s);
                            var l, c = Math.pow(10, -r), u = a / c;
                            if (1.5 > u ? l = 1 : 3 > u ? (l = 2, u > 2.25 && (null == s || s >= r + 1) && (l = 2.5, ++r)) : l = 7.5 > u ? 5 : 10, l *= c, null != n.minTickSize && l < n.minTickSize && (l = n.minTickSize), e.delta = a, e.tickDecimals = Math.max(0, null != s ? s : r), e.tickSize = n.tickSize || l, "time" == n.mode && !e.tickGenerator)throw new Error("Time mode requires the flot.time plugin.");
                            if (e.tickGenerator || (e.tickGenerator = function (t) {
                                    var e, i = [], n = o(t.min, t.tickSize), a = 0, r = Number.NaN;
                                    do e = r, r = n + a * t.tickSize, i.push(r), ++a; while (r < t.max && r != e);
                                    return i
                                }, e.tickFormatter = function (t, e) {
                                    var i = e.tickDecimals ? Math.pow(10, e.tickDecimals) : 1, o = "" + Math.round(t * i) / i;
                                    if (null != e.tickDecimals) {
                                        var n = o.indexOf("."), a = -1 == n ? 0 : o.length - n - 1;
                                        if (a < e.tickDecimals)return (a ? o : o + ".") + ("" + i).substr(1, e.tickDecimals - a)
                                    }
                                    return o
                                }), t.isFunction(n.tickFormatter) && (e.tickFormatter = function (t, e) {
                                    return "" + n.tickFormatter(t, e)
                                }), null != n.alignTicksWithAxis) {
                                var h = ("x" == e.direction ? ue : he)[n.alignTicksWithAxis - 1];
                                if (h && h.used && h != e) {
                                    var p = e.tickGenerator(e);
                                    if (p.length > 0 && (null == n.min && (e.min = Math.min(e.min, p[0])), null == n.max && p.length > 1 && (e.max = Math.max(e.max, p[p.length - 1]))), e.tickGenerator = function (t) {
                                            var e, i, o = [];
                                            for (i = 0; i < h.ticks.length; ++i)e = (h.ticks[i].v - h.min) / (h.max - h.min), e = t.min + e * (t.max - t.min), o.push(e);
                                            return o
                                        }, !e.mode && null == n.tickDecimals) {
                                        var f = Math.max(0, -Math.floor(Math.log(e.delta) / Math.LN10) + 1), d = e.tickGenerator(e);
                                        d.length > 1 && /\..*0$/.test((d[1] - d[0]).toFixed(f)) || (e.tickDecimals = f)
                                    }
                                }
                            }
                        }

                        function D(e) {
                            var i = e.options.ticks, o = [];
                            null == i || "number" == typeof i && i > 0 ? o = e.tickGenerator(e) : i && (o = t.isFunction(i) ? i(e) : i);
                            var n, a;
                            for (e.ticks = [], n = 0; n < o.length; ++n) {
                                var r = null, s = o[n];
                                "object" == typeof s ? (a = +s[0], s.length > 1 && (r = s[1])) : a = +s, null == r && (r = e.tickFormatter(a, e)), isNaN(a) || e.ticks.push({
                                    v: a,
                                    label: r
                                })
                            }
                        }

                        function A(t, e) {
                            t.options.autoscaleMargin && e.length > 0 && (null == t.options.min && (t.min = Math.min(t.min, e[0].v)), null == t.options.max && e.length > 1 && (t.max = Math.max(t.max, e[e.length - 1].v)))
                        }

                        function W() {
                            ae.clear(), s(me.drawBackground, [le]);
                            var t = ne.grid;
                            t.show && t.backgroundColor && I(), t.show && !t.aboveData && q();
                            for (var e = 0; e < oe.length; ++e)s(me.drawSeries, [le, oe[e]]), R(oe[e]);
                            s(me.draw, [le]), t.show && t.aboveData && q(), ae.render(), J()
                        }

                        function P(t, e) {
                            for (var i, o, n, a, r = f(), s = 0; s < r.length; ++s)if (i = r[s], i.direction == e && (a = e + i.n + "axis", t[a] || 1 != i.n || (a = e + "axis"), t[a])) {
                                o = t[a].from, n = t[a].to;
                                break
                            }
                            if (t[a] || (i = "x" == e ? ue[0] : he[0], o = t[e + "1"], n = t[e + "2"]), null != o && null != n && o > n) {
                                var l = o;
                                o = n, n = l
                            }
                            return {from: o, to: n, axis: i}
                        }

                        function I() {
                            le.save(), le.translate(pe.left, pe.top), le.fillStyle = ie(ne.grid.backgroundColor, de, 0, "rgba(255, 255, 255, 0)"), le.fillRect(0, 0, fe, de), le.restore()
                        }

                        function q() {
                            var e, i, o, n;
                            le.save(), le.translate(pe.left, pe.top);
                            var a = ne.grid.markings;
                            if (a)for (t.isFunction(a) && (i = ge.getAxes(), i.xmin = i.xaxis.min, i.xmax = i.xaxis.max, i.ymin = i.yaxis.min, i.ymax = i.yaxis.max, a = a(i)), e = 0; e < a.length; ++e) {
                                var r = a[e], s = P(r, "x"), l = P(r, "y");
                                if (null == s.from && (s.from = s.axis.min), null == s.to && (s.to = s.axis.max), null == l.from && (l.from = l.axis.min), null == l.to && (l.to = l.axis.max), !(s.to < s.axis.min || s.from > s.axis.max || l.to < l.axis.min || l.from > l.axis.max)) {
                                    s.from = Math.max(s.from, s.axis.min), s.to = Math.min(s.to, s.axis.max), l.from = Math.max(l.from, l.axis.min), l.to = Math.min(l.to, l.axis.max);
                                    var c = s.from === s.to, u = l.from === l.to;
                                    if (!c || !u)if (s.from = Math.floor(s.axis.p2c(s.from)), s.to = Math.floor(s.axis.p2c(s.to)), l.from = Math.floor(l.axis.p2c(l.from)), l.to = Math.floor(l.axis.p2c(l.to)), c || u) {
                                        var h = r.lineWidth || ne.grid.markingsLineWidth, p = h % 2 ? .5 : 0;
                                        le.beginPath(), le.strokeStyle = r.color || ne.grid.markingsColor, le.lineWidth = h, c ? (le.moveTo(s.to + p, l.from), le.lineTo(s.to + p, l.to)) : (le.moveTo(s.from, l.to + p), le.lineTo(s.to, l.to + p)), le.stroke()
                                    } else le.fillStyle = r.color || ne.grid.markingsColor, le.fillRect(s.from, l.to, s.to - s.from, l.from - l.to)
                                }
                            }
                            i = f(), o = ne.grid.borderWidth;
                            for (var d = 0; d < i.length; ++d) {
                                var m, g, x, v, b = i[d], y = b.box, k = b.tickLength;
                                if (b.show && 0 != b.ticks.length) {
                                    for (le.lineWidth = 1, "x" == b.direction ? (m = 0, g = "full" == k ? "top" == b.position ? 0 : de : y.top - pe.top + ("top" == b.position ? y.height : 0)) : (g = 0, m = "full" == k ? "left" == b.position ? 0 : fe : y.left - pe.left + ("left" == b.position ? y.width : 0)), b.innermost || (le.strokeStyle = b.options.color, le.beginPath(), x = v = 0, "x" == b.direction ? x = fe + 1 : v = de + 1, 1 == le.lineWidth && ("x" == b.direction ? g = Math.floor(g) + .5 : m = Math.floor(m) + .5), le.moveTo(m, g), le.lineTo(m + x, g + v), le.stroke()), le.strokeStyle = b.options.tickColor, le.beginPath(), e = 0; e < b.ticks.length; ++e) {
                                        var w = b.ticks[e].v;
                                        x = v = 0, isNaN(w) || w < b.min || w > b.max || "full" == k && ("object" == typeof o && o[b.position] > 0 || o > 0) && (w == b.min || w == b.max) || ("x" == b.direction ? (m = b.p2c(w), v = "full" == k ? -de : k, "top" == b.position && (v = -v)) : (g = b.p2c(w), x = "full" == k ? -fe : k, "left" == b.position && (x = -x)), 1 == le.lineWidth && ("x" == b.direction ? m = Math.floor(m) + .5 : g = Math.floor(g) + .5), le.moveTo(m, g), le.lineTo(m + x, g + v))
                                    }
                                    le.stroke()
                                }
                            }
                            o && (n = ne.grid.borderColor, "object" == typeof o || "object" == typeof n ? ("object" != typeof o && (o = {
                                    top: o,
                                    right: o,
                                    bottom: o,
                                    left: o
                                }), "object" != typeof n && (n = {
                                    top: n,
                                    right: n,
                                    bottom: n,
                                    left: n
                                }), o.top > 0 && (le.strokeStyle = n.top, le.lineWidth = o.top, le.beginPath(), le.moveTo(0 - o.left, 0 - o.top / 2), le.lineTo(fe, 0 - o.top / 2), le.stroke()), o.right > 0 && (le.strokeStyle = n.right, le.lineWidth = o.right, le.beginPath(), le.moveTo(fe + o.right / 2, 0 - o.top), le.lineTo(fe + o.right / 2, de), le.stroke()), o.bottom > 0 && (le.strokeStyle = n.bottom, le.lineWidth = o.bottom, le.beginPath(), le.moveTo(fe + o.right, de + o.bottom / 2), le.lineTo(0, de + o.bottom / 2), le.stroke()), o.left > 0 && (le.strokeStyle = n.left, le.lineWidth = o.left, le.beginPath(), le.moveTo(0 - o.left / 2, de + o.bottom), le.lineTo(0 - o.left / 2, 0), le.stroke())) : (le.lineWidth = o, le.strokeStyle = ne.grid.borderColor, le.strokeRect(-o / 2, -o / 2, fe + o, de + o))), le.restore()
                        }

                        function N() {
                            t.each(f(), function (t, e) {
                                var i, o, n, a, r, s = e.box, l = e.direction + "Axis " + e.direction + e.n + "Axis", c = "flot-" + e.direction + "-axis flot-" + e.direction + e.n + "-axis " + l, u = e.options.font || "flot-tick-label tickLabel";
                                if (ae.removeText(c), e.show && 0 != e.ticks.length)for (var h = 0; h < e.ticks.length; ++h)i = e.ticks[h], !i.label || i.v < e.min || i.v > e.max || ("x" == e.direction ? (a = "center", o = pe.left + e.p2c(i.v), "bottom" == e.position ? n = s.top + s.padding : (n = s.top + s.height - s.padding, r = "bottom")) : (r = "middle", n = pe.top + e.p2c(i.v), "left" == e.position ? (o = s.left + s.width - s.padding, a = "right") : o = s.left + s.padding), ae.addText(c, o, n, i.label, u, null, null, a, r))
                            })
                        }

                        function R(t) {
                            t.lines.show && H(t), t.bars.show && E(t), t.points.show && Y(t)
                        }

                        function H(t) {
                            function e(t, e, i, o, n) {
                                var a = t.points, r = t.pointsize, s = null, l = null;
                                le.beginPath();
                                for (var c = r; c < a.length; c += r) {
                                    var u = a[c - r], h = a[c - r + 1], p = a[c], f = a[c + 1];
                                    if (null != u && null != p) {
                                        if (f >= h && h < n.min) {
                                            if (f < n.min)continue;
                                            u = (n.min - h) / (f - h) * (p - u) + u, h = n.min
                                        } else if (h >= f && f < n.min) {
                                            if (h < n.min)continue;
                                            p = (n.min - h) / (f - h) * (p - u) + u, f = n.min
                                        }
                                        if (h >= f && h > n.max) {
                                            if (f > n.max)continue;
                                            u = (n.max - h) / (f - h) * (p - u) + u, h = n.max
                                        } else if (f >= h && f > n.max) {
                                            if (h > n.max)continue;
                                            p = (n.max - h) / (f - h) * (p - u) + u, f = n.max
                                        }
                                        if (p >= u && u < o.min) {
                                            if (p < o.min)continue;
                                            h = (o.min - u) / (p - u) * (f - h) + h, u = o.min
                                        } else if (u >= p && p < o.min) {
                                            if (u < o.min)continue;
                                            f = (o.min - u) / (p - u) * (f - h) + h, p = o.min
                                        }
                                        if (u >= p && u > o.max) {
                                            if (p > o.max)continue;
                                            h = (o.max - u) / (p - u) * (f - h) + h, u = o.max
                                        } else if (p >= u && p > o.max) {
                                            if (u > o.max)continue;
                                            f = (o.max - u) / (p - u) * (f - h) + h, p = o.max
                                        }
                                        (u != s || h != l) && le.moveTo(o.p2c(u) + e, n.p2c(h) + i), s = p, l = f, le.lineTo(o.p2c(p) + e, n.p2c(f) + i)
                                    }
                                }
                                le.stroke()
                            }

                            function i(t, e, i) {
                                for (var o = t.points, n = t.pointsize, a = Math.min(Math.max(0, i.min), i.max), r = 0, s = !1, l = 1, c = 0, u = 0; ;) {
                                    if (n > 0 && r > o.length + n)break;
                                    r += n;
                                    var h = o[r - n], p = o[r - n + l], f = o[r], d = o[r + l];
                                    if (s) {
                                        if (n > 0 && null != h && null == f) {
                                            u = r, n = -n, l = 2;
                                            continue
                                        }
                                        if (0 > n && r == c + n) {
                                            le.fill(), s = !1, n = -n, l = 1, r = c = u + n;
                                            continue
                                        }
                                    }
                                    if (null != h && null != f) {
                                        if (f >= h && h < e.min) {
                                            if (f < e.min)continue;
                                            p = (e.min - h) / (f - h) * (d - p) + p, h = e.min
                                        } else if (h >= f && f < e.min) {
                                            if (h < e.min)continue;
                                            d = (e.min - h) / (f - h) * (d - p) + p, f = e.min
                                        }
                                        if (h >= f && h > e.max) {
                                            if (f > e.max)continue;
                                            p = (e.max - h) / (f - h) * (d - p) + p, h = e.max
                                        } else if (f >= h && f > e.max) {
                                            if (h > e.max)continue;
                                            d = (e.max - h) / (f - h) * (d - p) + p, f = e.max
                                        }
                                        if (s || (le.beginPath(), le.moveTo(e.p2c(h), i.p2c(a)), s = !0), p >= i.max && d >= i.max) le.lineTo(e.p2c(h), i.p2c(i.max)), le.lineTo(e.p2c(f), i.p2c(i.max)); else if (p <= i.min && d <= i.min) le.lineTo(e.p2c(h), i.p2c(i.min)), le.lineTo(e.p2c(f), i.p2c(i.min)); else {
                                            var m = h, g = f;
                                            d >= p && p < i.min && d >= i.min ? (h = (i.min - p) / (d - p) * (f - h) + h, p = i.min) : p >= d && d < i.min && p >= i.min && (f = (i.min - p) / (d - p) * (f - h) + h, d = i.min), p >= d && p > i.max && d <= i.max ? (h = (i.max - p) / (d - p) * (f - h) + h, p = i.max) : d >= p && d > i.max && p <= i.max && (f = (i.max - p) / (d - p) * (f - h) + h, d = i.max), h != m && le.lineTo(e.p2c(m), i.p2c(p)), le.lineTo(e.p2c(h), i.p2c(p)), le.lineTo(e.p2c(f), i.p2c(d)), f != g && (le.lineTo(e.p2c(f), i.p2c(d)), le.lineTo(e.p2c(g), i.p2c(d)))
                                        }
                                    }
                                }
                            }

                            le.save(), le.translate(pe.left, pe.top), le.lineJoin = "round";
                            var o = t.lines.lineWidth, n = t.shadowSize;
                            if (o > 0 && n > 0) {
                                le.lineWidth = n, le.strokeStyle = "rgba(0,0,0,0.1)";
                                var a = Math.PI / 18;
                                e(t.datapoints, Math.sin(a) * (o / 2 + n / 2), Math.cos(a) * (o / 2 + n / 2), t.xaxis, t.yaxis), le.lineWidth = n / 2, e(t.datapoints, Math.sin(a) * (o / 2 + n / 4), Math.cos(a) * (o / 2 + n / 4), t.xaxis, t.yaxis)
                            }
                            le.lineWidth = o, le.strokeStyle = t.color;
                            var r = X(t.lines, t.color, 0, de);
                            r && (le.fillStyle = r, i(t.datapoints, t.xaxis, t.yaxis)), o > 0 && e(t.datapoints, 0, 0, t.xaxis, t.yaxis), le.restore()
                        }

                        function Y(t) {
                            function e(t, e, i, o, n, a, r, s) {
                                for (var l = t.points, c = t.pointsize, u = 0; u < l.length; u += c) {
                                    var h = l[u], p = l[u + 1];
                                    null == h || h < a.min || h > a.max || p < r.min || p > r.max || (le.beginPath(), h = a.p2c(h), p = r.p2c(p) + o, "circle" == s ? le.arc(h, p, e, 0, n ? Math.PI : 2 * Math.PI, !1) : s(le, h, p, e, n), le.closePath(), i && (le.fillStyle = i, le.fill()), le.stroke())
                                }
                            }

                            le.save(), le.translate(pe.left, pe.top);
                            var i = t.points.lineWidth, o = t.shadowSize, n = t.points.radius, a = t.points.symbol;
                            if (0 == i && (i = 1e-4), i > 0 && o > 0) {
                                var r = o / 2;
                                le.lineWidth = r, le.strokeStyle = "rgba(0,0,0,0.1)", e(t.datapoints, n, null, r + r / 2, !0, t.xaxis, t.yaxis, a), le.strokeStyle = "rgba(0,0,0,0.2)", e(t.datapoints, n, null, r / 2, !0, t.xaxis, t.yaxis, a)
                            }
                            le.lineWidth = i, le.strokeStyle = t.color, e(t.datapoints, n, X(t.points, t.color), 0, !1, t.xaxis, t.yaxis, a), le.restore()
                        }

                        function L(t, e, i, o, n, a, r, s, l, c, u) {
                            var h, p, f, d, m, g, x, v, b;
                            c ? (v = g = x = !0, m = !1, h = i, p = t, d = e + o, f = e + n, h > p && (b = p, p = h, h = b, m = !0, g = !1)) : (m = g = x = !0, v = !1, h = t + o, p = t + n, f = i, d = e, f > d && (b = d, d = f, f = b, v = !0, x = !1)), p < r.min || h > r.max || d < s.min || f > s.max || (h < r.min && (h = r.min, m = !1), p > r.max && (p = r.max, g = !1), f < s.min && (f = s.min, v = !1), d > s.max && (d = s.max, x = !1), h = r.p2c(h), f = s.p2c(f), p = r.p2c(p), d = s.p2c(d), a && (l.fillStyle = a(f, d), l.fillRect(h, d, p - h, f - d)), u > 0 && (m || g || x || v) && (l.beginPath(), l.moveTo(h, f), m ? l.lineTo(h, d) : l.moveTo(h, d), x ? l.lineTo(p, d) : l.moveTo(p, d), g ? l.lineTo(p, f) : l.moveTo(p, f), v ? l.lineTo(h, f) : l.moveTo(h, f), l.stroke()))
                        }

                        function E(t) {
                            function e(e, i, o, n, a, r) {
                                for (var s = e.points, l = e.pointsize, c = 0; c < s.length; c += l)null != s[c] && L(s[c], s[c + 1], s[c + 2], i, o, n, a, r, le, t.bars.horizontal, t.bars.lineWidth)
                            }

                            le.save(), le.translate(pe.left, pe.top), le.lineWidth = t.bars.lineWidth, le.strokeStyle = t.color;
                            var i;
                            switch (t.bars.align) {
                                case"left":
                                    i = 0;
                                    break;
                                case"right":
                                    i = -t.bars.barWidth;
                                    break;
                                default:
                                    i = -t.bars.barWidth / 2
                            }
                            var o = t.bars.fill ? function (e, i) {
                                    return X(t.bars, t.color, e, i)
                                } : null;
                            e(t.datapoints, i, i + t.bars.barWidth, o, t.xaxis, t.yaxis), le.restore()
                        }

                        function X(e, i, o, n) {
                            var a = e.fill;
                            if (!a)return null;
                            if (e.fillColor)return ie(e.fillColor, o, n, i);
                            var r = t.color.parse(i);
                            return r.a = "number" == typeof a ? a : .4, r.normalize(), r.toString()
                        }

                        function j() {
                            if (null != ne.legend.container ? t(ne.legend.container).html("") : i.find(".legend").remove(), ne.legend.show) {
                                for (var e, o, n = [], a = [], r = !1, s = ne.legend.labelFormatter, l = 0; l < oe.length; ++l)e = oe[l], e.label && (o = s ? s(e.label, e) : e.label, o && a.push({
                                    label: o,
                                    color: e.color
                                }));
                                if (ne.legend.sorted)if (t.isFunction(ne.legend.sorted)) a.sort(ne.legend.sorted); else if ("reverse" == ne.legend.sorted) a.reverse(); else {
                                    var c = "descending" != ne.legend.sorted;
                                    a.sort(function (t, e) {
                                        return t.label == e.label ? 0 : t.label < e.label != c ? 1 : -1
                                    })
                                }
                                for (var l = 0; l < a.length; ++l) {
                                    var u = a[l];
                                    l % ne.legend.noColumns == 0 && (r && n.push("</tr>"), n.push("<tr>"), r = !0), n.push('<td class="legendColorBox"><div style="border:1px solid ' + ne.legend.labelBoxBorderColor + ';padding:1px"><div style="width:4px;height:0;border:5px solid ' + u.color + ';overflow:hidden"></div></div></td><td class="legendLabel">' + u.label + "</td>")
                                }
                                if (r && n.push("</tr>"), 0 != n.length) {
                                    var h = '<table style="font-size:smaller;color:' + ne.grid.color + '">' + n.join("") + "</table>";
                                    if (null != ne.legend.container) t(ne.legend.container).html(h); else {
                                        var p = "", f = ne.legend.position, d = ne.legend.margin;
                                        null == d[0] && (d = [d, d]), "n" == f.charAt(0) ? p += "top:" + (d[1] + pe.top) + "px;" : "s" == f.charAt(0) && (p += "bottom:" + (d[1] + pe.bottom) + "px;"), "e" == f.charAt(1) ? p += "right:" + (d[0] + pe.right) + "px;" : "w" == f.charAt(1) && (p += "left:" + (d[0] + pe.left) + "px;");
                                        var m = t('<div class="legend">' + h.replace('style="', 'style="position:absolute;' + p + ";") + "</div>").appendTo(i);
                                        if (0 != ne.legend.backgroundOpacity) {
                                            var g = ne.legend.backgroundColor;
                                            null == g && (g = ne.grid.backgroundColor, g = g && "string" == typeof g ? t.color.parse(g) : t.color.extract(m, "background-color"), g.a = 1, g = g.toString());
                                            var x = m.children();
                                            t('<div style="position:absolute;width:' + x.width() + "px;height:" + x.height() + "px;" + p + "background-color:" + g + ';"> </div>').prependTo(m).css("opacity", ne.legend.backgroundOpacity)
                                        }
                                    }
                                }
                            }
                        }

                        function G(t, e, i) {
                            var o, n, a, r = ne.grid.mouseActiveRadius, s = r * r + 1, l = null;
                            for (o = oe.length - 1; o >= 0; --o)if (i(oe[o])) {
                                var c = oe[o], u = c.xaxis, h = c.yaxis, p = c.datapoints.points, f = u.c2p(t), d = h.c2p(e), m = r / u.scale, g = r / h.scale;
                                if (a = c.datapoints.pointsize, u.options.inverseTransform && (m = Number.MAX_VALUE), h.options.inverseTransform && (g = Number.MAX_VALUE), c.lines.show || c.points.show)for (n = 0; n < p.length; n += a) {
                                    var x = p[n], v = p[n + 1];
                                    if (null != x && !(x - f > m || -m > x - f || v - d > g || -g > v - d)) {
                                        var b = Math.abs(u.p2c(x) - t), y = Math.abs(h.p2c(v) - e), k = b * b + y * y;
                                        s > k && (s = k, l = [o, n / a])
                                    }
                                }
                                if (c.bars.show && !l) {
                                    var w, M;
                                    switch (c.bars.align) {
                                        case"left":
                                            w = 0;
                                            break;
                                        case"right":
                                            w = -c.bars.barWidth;
                                            break;
                                        default:
                                            w = -c.bars.barWidth / 2
                                    }
                                    for (M = w + c.bars.barWidth, n = 0; n < p.length; n += a) {
                                        var x = p[n], v = p[n + 1], T = p[n + 2];
                                        null != x && (oe[o].bars.horizontal ? f <= Math.max(T, x) && f >= Math.min(T, x) && d >= v + w && v + M >= d : f >= x + w && x + M >= f && d >= Math.min(T, v) && d <= Math.max(T, v)) && (l = [o, n / a])
                                    }
                                }
                            }
                            return l ? (o = l[0], n = l[1], a = oe[o].datapoints.pointsize, {
                                    datapoint: oe[o].datapoints.points.slice(n * a, (n + 1) * a),
                                    dataIndex: n,
                                    series: oe[o],
                                    seriesIndex: o
                                }) : null
                        }

                        function _(t) {
                            ne.grid.hoverable && $("plothover", t, function (t) {
                                return 0 != t.hoverable
                            })
                        }

                        function B(t) {
                            ne.grid.hoverable && $("plothover", t, function () {
                                return !1
                            })
                        }

                        function V(t) {
                            $("plotclick", t, function (t) {
                                return 0 != t.clickable
                            })
                        }

                        function $(t, e, o) {
                            var n = se.offset(), a = e.pageX - n.left - pe.left, r = e.pageY - n.top - pe.top, s = d({
                                left: a,
                                top: r
                            });
                            s.pageX = e.pageX, s.pageY = e.pageY;
                            var l = G(a, r, o);
                            if (l && (l.pageX = parseInt(l.series.xaxis.p2c(l.datapoint[0]) + n.left + pe.left, 10), l.pageY = parseInt(l.series.yaxis.p2c(l.datapoint[1]) + n.top + pe.top, 10)), ne.grid.autoHighlight) {
                                for (var c = 0; c < xe.length; ++c) {
                                    var u = xe[c];
                                    u.auto != t || l && u.series == l.series && u.point[0] == l.datapoint[0] && u.point[1] == l.datapoint[1] || Z(u.series, u.point)
                                }
                                l && Q(l.series, l.datapoint, t)
                            }
                            i.trigger(t, [s, l])
                        }

                        function J() {
                            var t = ne.interaction.redrawOverlayInterval;
                            return -1 == t ? void U() : void(ve || (ve = setTimeout(U, t)))
                        }

                        function U() {
                            ve = null, ce.save(), re.clear(), ce.translate(pe.left, pe.top);
                            var t, e;
                            for (t = 0; t < xe.length; ++t)e = xe[t], e.series.bars.show ? ee(e.series, e.point) : te(e.series, e.point);
                            ce.restore(), s(me.drawOverlay, [ce])
                        }

                        function Q(t, e, i) {
                            if ("number" == typeof t && (t = oe[t]), "number" == typeof e) {
                                var o = t.datapoints.pointsize;
                                e = t.datapoints.points.slice(o * e, o * (e + 1))
                            }
                            var n = K(t, e);
                            -1 == n ? (xe.push({series: t, point: e, auto: i}), J()) : i || (xe[n].auto = !1)
                        }

                        function Z(t, e) {
                            if (null == t && null == e)return xe = [], void J();
                            if ("number" == typeof t && (t = oe[t]), "number" == typeof e) {
                                var i = t.datapoints.pointsize;
                                e = t.datapoints.points.slice(i * e, i * (e + 1))
                            }
                            var o = K(t, e);
                            -1 != o && (xe.splice(o, 1), J())
                        }

                        function K(t, e) {
                            for (var i = 0; i < xe.length; ++i) {
                                var o = xe[i];
                                if (o.series == t && o.point[0] == e[0] && o.point[1] == e[1])return i
                            }
                            return -1
                        }

                        function te(e, i) {
                            var o = i[0], n = i[1], a = e.xaxis, r = e.yaxis, s = "string" == typeof e.highlightColor ? e.highlightColor : t.color.parse(e.color).scale("a", .5).toString();
                            if (!(o < a.min || o > a.max || n < r.min || n > r.max)) {
                                var l = e.points.radius + e.points.lineWidth / 2;
                                ce.lineWidth = l, ce.strokeStyle = s;
                                var c = 1.5 * l;
                                o = a.p2c(o), n = r.p2c(n), ce.beginPath(), "circle" == e.points.symbol ? ce.arc(o, n, c, 0, 2 * Math.PI, !1) : e.points.symbol(ce, o, n, c, !1), ce.closePath(), ce.stroke()
                            }
                        }

                        function ee(e, i) {
                            var o, n = "string" == typeof e.highlightColor ? e.highlightColor : t.color.parse(e.color).scale("a", .5).toString(), a = n;
                            switch (e.bars.align) {
                                case"left":
                                    o = 0;
                                    break;
                                case"right":
                                    o = -e.bars.barWidth;
                                    break;
                                default:
                                    o = -e.bars.barWidth / 2
                            }
                            ce.lineWidth = e.bars.lineWidth, ce.strokeStyle = n, L(i[0], i[1], i[2] || 0, o, o + e.bars.barWidth, function () {
                                return a
                            }, e.xaxis, e.yaxis, ce, e.bars.horizontal, e.bars.lineWidth)
                        }

                        function ie(e, i, o, n) {
                            if ("string" == typeof e)return e;
                            for (var a = le.createLinearGradient(0, o, 0, i), r = 0, s = e.colors.length; s > r; ++r) {
                                var l = e.colors[r];
                                if ("string" != typeof l) {
                                    var c = t.color.parse(n);
                                    null != l.brightness && (c = c.scale("rgb", l.brightness)), null != l.opacity && (c.a *= l.opacity), l = c.toString()
                                }
                                a.addColorStop(r / (s - 1), l)
                            }
                            return a
                        }

                        var oe = [], ne = {
                            colors: ["#edc240", "#afd8f8", "#cb4b4b", "#4da74d", "#9440ed"],
                            legend: {
                                show: !0,
                                noColumns: 1,
                                labelFormatter: null,
                                labelBoxBorderColor: "#ccc",
                                container: null,
                                position: "ne",
                                margin: 5,
                                backgroundColor: null,
                                backgroundOpacity: .85,
                                sorted: null
                            },
                            xaxis: {
                                show: null,
                                position: "bottom",
                                mode: null,
                                font: null,
                                color: null,
                                tickColor: null,
                                transform: null,
                                inverseTransform: null,
                                min: null,
                                max: null,
                                autoscaleMargin: null,
                                ticks: null,
                                tickFormatter: null,
                                labelWidth: null,
                                labelHeight: null,
                                reserveSpace: null,
                                tickLength: null,
                                alignTicksWithAxis: null,
                                tickDecimals: null,
                                tickSize: null,
                                minTickSize: null
                            },
                            yaxis: {autoscaleMargin: .02, position: "left"},
                            xaxes: [],
                            yaxes: [],
                            series: {
                                points: {
                                    show: !1,
                                    radius: 3,
                                    lineWidth: 2,
                                    fill: !0,
                                    fillColor: "#ffffff",
                                    symbol: "circle"
                                },
                                lines: {lineWidth: 2, fill: !1, fillColor: null, steps: !1},
                                bars: {
                                    show: !1,
                                    lineWidth: 2,
                                    barWidth: 1,
                                    fill: !0,
                                    fillColor: null,
                                    align: "left",
                                    horizontal: !1,
                                    zero: !0
                                },
                                shadowSize: 3,
                                highlightColor: null
                            },
                            grid: {
                                show: !0,
                                aboveData: !1,
                                color: "#545454",
                                backgroundColor: null,
                                borderColor: null,
                                tickColor: null,
                                margin: 0,
                                labelMargin: 5,
                                axisMargin: 8,
                                borderWidth: 2,
                                minBorderMargin: null,
                                markings: null,
                                markingsColor: "#f4f4f4",
                                markingsLineWidth: 2,
                                clickable: !1,
                                hoverable: !1,
                                autoHighlight: !0,
                                mouseActiveRadius: 10
                            },
                            interaction: {redrawOverlayInterval: 1e3 / 60},
                            hooks: {}
                        }, ae = null, re = null, se = null, le = null, ce = null, ue = [], he = [], pe = {
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0
                        }, fe = 0, de = 0, me = {
                            processOptions: [],
                            processRawData: [],
                            processDatapoints: [],
                            processOffset: [],
                            drawBackground: [],
                            drawSeries: [],
                            draw: [],
                            bindEvents: [],
                            drawOverlay: [],
                            shutdown: []
                        }, ge = this;
                        ge.setData = u, ge.setupGrid = C, ge.draw = W, ge.getPlaceholder = function () {
                            return i
                        }, ge.getCanvas = function () {
                            return ae.element
                        }, ge.getPlotOffset = function () {
                            return pe
                        }, ge.width = function () {
                            return fe
                        }, ge.height = function () {
                            return de
                        }, ge.offset = function () {
                            var t = se.offset();
                            return t.left += pe.left, t.top += pe.top, t
                        }, ge.getData = function () {
                            return oe
                        }, ge.getAxes = function () {
                            var e = {};
                            return t.each(ue.concat(he), function (t, i) {
                                i && (e[i.direction + (1 != i.n ? i.n : "") + "axis"] = i)
                            }), e
                        }, ge.getXAxes = function () {
                            return ue
                        }, ge.getYAxes = function () {
                            return he
                        }, ge.c2p = d, ge.p2c = m, ge.getOptions = function () {
                            return ne
                        }, ge.highlight = Q, ge.unhighlight = Z, ge.triggerRedrawOverlay = J, ge.pointOffset = function (t) {
                            return {
                                left: parseInt(ue[p(t, "x") - 1].p2c(+t.x) + pe.left, 10),
                                top: parseInt(he[p(t, "y") - 1].p2c(+t.y) + pe.top, 10)
                            }
                        }, ge.shutdown = k, ge.destroy = function () {
                            k(), i.removeData("plot").empty(), oe = [], ne = null, ae = null, re = null, se = null, le = null, ce = null, ue = [], he = [], me = null, xe = [], ge = null
                        }, ge.resize = function () {
                            var t = i.width(), e = i.height();
                            ae.resize(t, e), re.resize(t, e)
                        }, ge.hooks = me, l(ge), c(a), b(), u(n), C(), W(), y();
                        var xe = [], ve = null
                    }

                    function o(t, e) {
                        return e * Math.floor(t / e)
                    }

                    var n = Object.prototype.hasOwnProperty;
                    t.fn.detach || (t.fn.detach = function () {
                        return this.each(function () {
                            this.parentNode && this.parentNode.removeChild(this)
                        })
                    }), e.prototype.resize = function (t, e) {
                        if (0 >= t || 0 >= e)throw new Error("Invalid dimensions for plot, width = " + t + ", height = " + e);
                        var i = this.element, o = this.context, n = this.pixelRatio;
                        this.width != t && (i.width = t * n, i.style.width = t + "px", this.width = t), this.height != e && (i.height = e * n, i.style.height = e + "px", this.height = e), o.restore(), o.save(), o.scale(n, n)
                    }, e.prototype.clear = function () {
                        this.context.clearRect(0, 0, this.width, this.height)
                    }, e.prototype.render = function () {
                        var t = this._textCache;
                        for (var e in t)if (n.call(t, e)) {
                            var i = this.getTextLayer(e), o = t[e];
                            i.hide();
                            for (var a in o)if (n.call(o, a)) {
                                var r = o[a];
                                for (var s in r)if (n.call(r, s)) {
                                    for (var l, c = r[s].positions, u = 0; l = c[u]; u++)l.active ? l.rendered || (i.append(l.element), l.rendered = !0) : (c.splice(u--, 1), l.rendered && l.element.detach());
                                    0 == c.length && delete r[s]
                                }
                            }
                            i.show()
                        }
                    }, e.prototype.getTextLayer = function (e) {
                        var i = this.text[e];
                        return null == i && (null == this.textContainer && (this.textContainer = t("<div class='flot-text'></div>").css({
                            position: "absolute",
                            top: 0,
                            left: 0,
                            bottom: 0,
                            right: 0,
                            "font-size": "smaller",
                            color: "#545454"
                        }).insertAfter(this.element)), i = this.text[e] = t("<div></div>").addClass(e).css({
                            position: "absolute",
                            top: 0,
                            left: 0,
                            bottom: 0,
                            right: 0
                        }).appendTo(this.textContainer)), i
                    }, e.prototype.getTextInfo = function (e, i, o, n, a) {
                        var r, s, l, c;
                        if (i = "" + i, r = "object" == typeof o ? o.style + " " + o.variant + " " + o.weight + " " + o.size + "px/" + o.lineHeight + "px " + o.family : o, s = this._textCache[e], null == s && (s = this._textCache[e] = {}), l = s[r], null == l && (l = s[r] = {}), c = l[i], null == c) {
                            var u = t("<div></div>").html(i).css({
                                position: "absolute",
                                "max-width": a,
                                top: -9999
                            }).appendTo(this.getTextLayer(e));
                            "object" == typeof o ? u.css({
                                    font: r,
                                    color: o.color
                                }) : "string" == typeof o && u.addClass(o), c = l[i] = {
                                width: u.outerWidth(!0),
                                height: u.outerHeight(!0),
                                element: u,
                                positions: []
                            }, u.detach()
                        }
                        return c
                    }, e.prototype.addText = function (t, e, i, o, n, a, r, s, l) {
                        var c = this.getTextInfo(t, o, n, a, r), u = c.positions;
                        "center" == s ? e -= c.width / 2 : "right" == s && (e -= c.width), "middle" == l ? i -= c.height / 2 : "bottom" == l && (i -= c.height);
                        for (var h, p = 0; h = u[p]; p++)if (h.x == e && h.y == i)return void(h.active = !0);
                        h = {
                            active: !0,
                            rendered: !1,
                            element: u.length ? c.element.clone() : c.element,
                            x: e,
                            y: i
                        }, u.push(h), h.element.css({top: Math.round(i), left: Math.round(e), "text-align": s})
                    }, e.prototype.removeText = function (t, e, i, o, a, r) {
                        if (null == o) {
                            var s = this._textCache[t];
                            if (null != s)for (var l in s)if (n.call(s, l)) {
                                var c = s[l];
                                for (var u in c)if (n.call(c, u))for (var h, p = c[u].positions, f = 0; h = p[f]; f++)h.active = !1
                            }
                        } else for (var h, p = this.getTextInfo(t, o, a, r).positions, f = 0; h = p[f]; f++)h.x == e && h.y == i && (h.active = !1)
                    }, t.plot = function (e, o, n) {
                        var a = new i(t(e), o, n, t.plot.plugins);
                        return a
                    }, t.plot.version = "0.8.3", t.plot.plugins = [], t.fn.plot = function (e, i) {
                        return this.each(function () {
                            t.plot(this, e, i)
                        })
                    }
                }(t)
            })
        }.apply(t, arguments)
    })
}(this), function (t) {
    define("flot-resize", ["flot"], function () {
        return function () {
            require(["jquery", "flot"], function (t) {
                !function (t, e, i) {
                    "$:nomunge";
                    function o(i) {
                        s === !0 && (s = i || 1);
                        for (var l = a.length - 1; l >= 0; l--) {
                            var p = t(a[l]);
                            if (p[0] == e || p.is(":visible")) {
                                var f = p.width(), d = p.height(), m = p.data(u);
                                !m || f === m.w && d === m.h || (p.trigger(c, [m.w = f, m.h = d]), s = i || !0)
                            } else m = p.data(u), m.w = 0, m.h = 0
                        }
                        null !== n && (s && (null == i || 1e3 > i - s) ? n = e.requestAnimationFrame(o) : (n = setTimeout(o, r[h]), s = !1))
                    }

                    var n, a = [], r = t.resize = t.extend(t.resize, {}), s = !1, l = "setTimeout", c = "resize", u = c + "-special-event", h = "pendingDelay", p = "activeDelay", f = "throttleWindow";
                    r[h] = 200, r[p] = 20, r[f] = !0, t.event.special[c] = {
                        setup: function () {
                            if (!r[f] && this[l])return !1;
                            var e = t(this);
                            a.push(this), e.data(u, {w: e.width(), h: e.height()}), 1 === a.length && (n = i, o())
                        }, teardown: function () {
                            if (!r[f] && this[l])return !1;
                            for (var e = t(this), i = a.length - 1; i >= 0; i--)if (a[i] == this) {
                                a.splice(i, 1);
                                break
                            }
                            e.removeData(u), a.length || (s ? cancelAnimationFrame(n) : clearTimeout(n), n = null)
                        }, add: function (e) {
                            function o(e, o, a) {
                                var r = t(this), s = r.data(u) || {};
                                s.w = o !== i ? o : r.width(), s.h = a !== i ? a : r.height(), n.apply(this, arguments)
                            }

                            if (!r[f] && this[l])return !1;
                            var n;
                            return t.isFunction(e) ? (n = e, o) : (n = e.handler, void(e.handler = o))
                        }
                    }, e.requestAnimationFrame || (e.requestAnimationFrame = function () {
                        return e.webkitRequestAnimationFrame || e.mozRequestAnimationFrame || e.oRequestAnimationFrame || e.msRequestAnimationFrame || function (t) {
                                return e.setTimeout(function () {
                                    t((new Date).getTime())
                                }, r[p])
                            }
                    }()), e.cancelAnimationFrame || (e.cancelAnimationFrame = function () {
                        return e.webkitCancelRequestAnimationFrame || e.mozCancelRequestAnimationFrame || e.oCancelRequestAnimationFrame || e.msCancelRequestAnimationFrame || clearTimeout
                    }())
                }(t, this), function (t) {
                    function e(t) {
                        function e() {
                            var e = t.getPlaceholder();
                            0 != e.width() && 0 != e.height() && (t.resize(), t.setupGrid(), t.draw())
                        }

                        function i(t) {
                            t.getPlaceholder().resize(e)
                        }

                        function o(t) {
                            t.getPlaceholder().unbind("resize", e)
                        }

                        t.hooks.bindEvents.push(i), t.hooks.shutdown.push(o)
                    }

                    var i = {};
                    t.plot.plugins.push({init: e, options: i, name: "resize", version: "1.0"})
                }(t)
            })
        }.apply(t, arguments)
    })
}(this), function (t) {
    define("flot-time", ["flot"], function () {
        return function () {
            require(["jquery", "flot"], function (t) {
                !function (t) {
                    function e(t, e) {
                        return e * Math.floor(t / e)
                    }

                    function i(t, e, i, o) {
                        if ("function" == typeof t.strftime)return t.strftime(e);
                        var n = function (t, e) {
                            return t = "" + t, e = "" + (null == e ? "0" : e), 1 == t.length ? e + t : t
                        }, a = [], r = !1, s = t.getHours(), l = 12 > s;
                        null == i && (i = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]), null == o && (o = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]);
                        var c;
                        c = s > 12 ? s - 12 : 0 == s ? 12 : s;
                        for (var u = 0; u < e.length; ++u) {
                            var h = e.charAt(u);
                            if (r) {
                                switch (h) {
                                    case"a":
                                        h = "" + o[t.getDay()];
                                        break;
                                    case"b":
                                        h = "" + i[t.getMonth()];
                                        break;
                                    case"d":
                                        h = n(t.getDate());
                                        break;
                                    case"e":
                                        h = n(t.getDate(), " ");
                                        break;
                                    case"h":
                                    case"H":
                                        h = n(s);
                                        break;
                                    case"I":
                                        h = n(c);
                                        break;
                                    case"l":
                                        h = n(c, " ");
                                        break;
                                    case"m":
                                        h = n(t.getMonth() + 1);
                                        break;
                                    case"M":
                                        h = n(t.getMinutes());
                                        break;
                                    case"q":
                                        h = "" + (Math.floor(t.getMonth() / 3) + 1);
                                        break;
                                    case"S":
                                        h = n(t.getSeconds());
                                        break;
                                    case"y":
                                        h = n(t.getFullYear() % 100);
                                        break;
                                    case"Y":
                                        h = "" + t.getFullYear();
                                        break;
                                    case"p":
                                        h = l ? "am" : "pm";
                                        break;
                                    case"P":
                                        h = l ? "AM" : "PM";
                                        break;
                                    case"w":
                                        h = "" + t.getDay()
                                }
                                a.push(h), r = !1
                            } else"%" == h ? r = !0 : a.push(h)
                        }
                        return a.join("")
                    }

                    function o(t) {
                        function e(t, e, i, o) {
                            t[e] = function () {
                                return i[o].apply(i, arguments)
                            }
                        }

                        var i = {date: t};
                        void 0 != t.strftime && e(i, "strftime", t, "strftime"), e(i, "getTime", t, "getTime"), e(i, "setTime", t, "setTime");
                        for (var o = ["Date", "Day", "FullYear", "Hours", "Milliseconds", "Minutes", "Month", "Seconds"], n = 0; n < o.length; n++)e(i, "get" + o[n], t, "getUTC" + o[n]), e(i, "set" + o[n], t, "setUTC" + o[n]);
                        return i
                    }

                    function n(t, e) {
                        if ("browser" == e.timezone)return new Date(t);
                        if (e.timezone && "utc" != e.timezone) {
                            if ("undefined" != typeof timezoneJS && "undefined" != typeof timezoneJS.Date) {
                                var i = new timezoneJS.Date;
                                return i.setTimezone(e.timezone), i.setTime(t), i
                            }
                            return o(new Date(t))
                        }
                        return o(new Date(t))
                    }

                    function a(o) {
                        o.hooks.processOptions.push(function (o) {
                            t.each(o.getAxes(), function (t, o) {
                                var a = o.options;
                                "time" == a.mode && (o.tickGenerator = function (t) {
                                    var i = [], o = n(t.min, a), r = 0, l = a.tickSize && "quarter" === a.tickSize[1] || a.minTickSize && "quarter" === a.minTickSize[1] ? u : c;
                                    null != a.minTickSize && (r = "number" == typeof a.tickSize ? a.tickSize : a.minTickSize[0] * s[a.minTickSize[1]]);
                                    for (var h = 0; h < l.length - 1 && !(t.delta < (l[h][0] * s[l[h][1]] + l[h + 1][0] * s[l[h + 1][1]]) / 2 && l[h][0] * s[l[h][1]] >= r); ++h);
                                    var p = l[h][0], f = l[h][1];
                                    if ("year" == f) {
                                        if (null != a.minTickSize && "year" == a.minTickSize[1]) p = Math.floor(a.minTickSize[0]); else {
                                            var d = Math.pow(10, Math.floor(Math.log(t.delta / s.year) / Math.LN10)), m = t.delta / s.year / d;
                                            p = 1.5 > m ? 1 : 3 > m ? 2 : 7.5 > m ? 5 : 10, p *= d
                                        }
                                        1 > p && (p = 1)
                                    }
                                    t.tickSize = a.tickSize || [p, f];
                                    var g = t.tickSize[0];
                                    f = t.tickSize[1];
                                    var x = g * s[f];
                                    "second" == f ? o.setSeconds(e(o.getSeconds(), g)) : "minute" == f ? o.setMinutes(e(o.getMinutes(), g)) : "hour" == f ? o.setHours(e(o.getHours(), g)) : "month" == f ? o.setMonth(e(o.getMonth(), g)) : "quarter" == f ? o.setMonth(3 * e(o.getMonth() / 3, g)) : "year" == f && o.setFullYear(e(o.getFullYear(), g)), o.setMilliseconds(0), x >= s.minute && o.setSeconds(0), x >= s.hour && o.setMinutes(0), x >= s.day && o.setHours(0), x >= 4 * s.day && o.setDate(1), x >= 2 * s.month && o.setMonth(e(o.getMonth(), 3)), x >= 2 * s.quarter && o.setMonth(e(o.getMonth(), 6)), x >= s.year && o.setMonth(0);
                                    var v, b = 0, y = Number.NaN;
                                    do if (v = y, y = o.getTime(), i.push(y), "month" == f || "quarter" == f)if (1 > g) {
                                        o.setDate(1);
                                        var k = o.getTime();
                                        o.setMonth(o.getMonth() + ("quarter" == f ? 3 : 1));
                                        var w = o.getTime();
                                        o.setTime(y + b * s.hour + (w - k) * g), b = o.getHours(), o.setHours(0)
                                    } else o.setMonth(o.getMonth() + g * ("quarter" == f ? 3 : 1)); else"year" == f ? o.setFullYear(o.getFullYear() + g) : o.setTime(y + x); while (y < t.max && y != v);
                                    return i
                                }, o.tickFormatter = function (t, e) {
                                    var o = n(t, e.options);
                                    if (null != a.timeformat)return i(o, a.timeformat, a.monthNames, a.dayNames);
                                    var r, l = e.options.tickSize && "quarter" == e.options.tickSize[1] || e.options.minTickSize && "quarter" == e.options.minTickSize[1], c = e.tickSize[0] * s[e.tickSize[1]], u = e.max - e.min, h = a.twelveHourClock ? " %p" : "", p = a.twelveHourClock ? "%I" : "%H";
                                    r = c < s.minute ? p + ":%M:%S" + h : c < s.day ? u < 2 * s.day ? p + ":%M" + h : "%b %d " + p + ":%M" + h : c < s.month ? "%b %d" : l && c < s.quarter || !l && c < s.year ? u < s.year ? "%b" : "%b %Y" : l && c < s.year ? u < s.year ? "Q%q" : "Q%q %Y" : "%Y";
                                    var f = i(o, r, a.monthNames, a.dayNames);
                                    return f
                                })
                            })
                        })
                    }

                    var r = {
                        xaxis: {
                            timezone: null,
                            timeformat: null,
                            twelveHourClock: !1,
                            monthNames: null
                        }
                    }, s = {
                        second: 1e3,
                        minute: 6e4,
                        hour: 36e5,
                        day: 864e5,
                        month: 2592e6,
                        quarter: 7776e6,
                        year: 525949.2 * 60 * 1e3
                    }, l = [[1, "second"], [2, "second"], [5, "second"], [10, "second"], [30, "second"], [1, "minute"], [2, "minute"], [5, "minute"], [10, "minute"], [30, "minute"], [1, "hour"], [2, "hour"], [4, "hour"], [8, "hour"], [12, "hour"], [1, "day"], [2, "day"], [3, "day"], [.25, "month"], [.5, "month"], [1, "month"], [2, "month"]], c = l.concat([[3, "month"], [6, "month"], [1, "year"]]), u = l.concat([[1, "quarter"], [2, "quarter"], [1, "year"]]);
                    t.plot.plugins.push({
                        init: a,
                        options: r,
                        name: "time",
                        version: "1.0"
                    }), t.plot.formatDate = i, t.plot.dateGenerator = n
                }(t)
            })
        }.apply(t, arguments)
    })
}(this), function (t) {
    define("flot-tooltip", ["flot"], function () {
        return function () {
            require(["jquery", "flot"], function (t) {
                !function (t) {
                    var e = {
                        tooltip: {
                            show: !1,
                            cssClass: "flotTip",
                            content: "%s | X: %x | Y: %y",
                            xDateFormat: null,
                            yDateFormat: null,
                            monthNames: null,
                            dayNames: null,
                            shifts: {x: 10, y: 20},
                            defaultTheme: !0,
                            lines: !1,
                            onHover: function () {
                            },
                            $compat: !1
                        }
                    };
                    e.tooltipOpts = e.tooltip;
                    var i = function (t) {
                        this.tipPosition = {x: 0, y: 0}, this.init(t)
                    };
                    i.prototype.init = function (e) {
                        function i(t) {
                            var i = {};
                            i.x = t.pageX, i.y = t.pageY, e.setTooltipPosition(i)
                        }

                        function o(i, o, a) {
                            var r = function (t, e, i, o) {
                                return Math.sqrt((i - t) * (i - t) + (o - e) * (o - e))
                            }, s = function (t, e, i, o, n, a, s) {
                                if (!s || (s = function (t, e, i, o, n, a) {
                                        if ("undefined" != typeof i)return {x: i, y: e};
                                        if ("undefined" != typeof o)return {x: t, y: o};
                                        var r, s = -1 / ((a - o) / (n - i));
                                        return {
                                            x: r = (n * (t * s - e + o) + i * (t * -s + e - a)) / (s * (n - i) + o - a),
                                            y: s * r - s * t + e
                                        }
                                    }(t, e, i, o, n, a), s.x >= Math.min(i, n) && s.x <= Math.max(i, n) && s.y >= Math.min(o, a) && s.y <= Math.max(o, a))) {
                                    var l = o - a, c = n - i, u = i * a - o * n;
                                    return Math.abs(l * t + c * e + u) / Math.sqrt(l * l + c * c)
                                }
                                var h = r(t, e, i, o), p = r(t, e, n, a);
                                return h > p ? p : h
                            };
                            if (a) e.showTooltip(a, o); else if (n.plotOptions.series.lines.show && n.tooltipOptions.lines === !0) {
                                var l = n.plotOptions.grid.mouseActiveRadius, c = {distance: l + 1};
                                t.each(e.getData(), function (t, i) {
                                    for (var n = 0, a = -1, l = 1; l < i.data.length; l++)i.data[l - 1][0] <= o.x && i.data[l][0] >= o.x && (n = l - 1, a = l);
                                    if (-1 === a)return void e.hideTooltip();
                                    var u = {x: i.data[n][0], y: i.data[n][1]}, h = {
                                        x: i.data[a][0],
                                        y: i.data[a][1]
                                    }, p = s(i.xaxis.p2c(o.x), i.yaxis.p2c(o.y), i.xaxis.p2c(u.x), i.yaxis.p2c(u.y), i.xaxis.p2c(h.x), i.yaxis.p2c(h.y), !1);
                                    if (p < c.distance) {
                                        var f = r(u.x, u.y, o.x, o.y) < r(o.x, o.y, h.x, h.y) ? n : a, d = (i.datapoints.pointsize, [o.x, u.y + (h.y - u.y) * ((o.x - u.x) / (h.x - u.x))]), m = {
                                            datapoint: d,
                                            dataIndex: f,
                                            series: i,
                                            seriesIndex: t
                                        };
                                        c = {distance: p, item: m}
                                    }
                                }), c.distance < l + 1 ? e.showTooltip(c.item, o) : e.hideTooltip()
                            } else e.hideTooltip()
                        }

                        var n = this, a = t.plot.plugins.length;
                        if (this.plotPlugins = [], a)for (var r = 0; a > r; r++)this.plotPlugins.push(t.plot.plugins[r].name);
                        e.hooks.bindEvents.push(function (e, a) {
                            if (n.plotOptions = e.getOptions(), "boolean" == typeof n.plotOptions.tooltip && (n.plotOptions.tooltipOpts.show = n.plotOptions.tooltip, n.plotOptions.tooltip = n.plotOptions.tooltipOpts, delete n.plotOptions.tooltipOpts), n.plotOptions.tooltip.show !== !1 && "undefined" != typeof n.plotOptions.tooltip.show) {
                                n.tooltipOptions = n.plotOptions.tooltip, n.tooltipOptions.$compat ? (n.wfunc = "width", n.hfunc = "height") : (n.wfunc = "innerWidth", n.hfunc = "innerHeight");
                                {
                                    n.getDomElement()
                                }
                                t(e.getPlaceholder()).bind("plothover", o), t(a).bind("mousemove", i)
                            }
                        }), e.hooks.shutdown.push(function (e, n) {
                            t(e.getPlaceholder()).unbind("plothover", o), t(n).unbind("mousemove", i)
                        }), e.setTooltipPosition = function (e) {
                            var i = n.getDomElement(), o = i.outerWidth() + n.tooltipOptions.shifts.x, a = i.outerHeight() + n.tooltipOptions.shifts.y;
                            e.x - t(window).scrollLeft() > t(window)[n.wfunc]() - o && (e.x -= o), e.y - t(window).scrollTop() > t(window)[n.hfunc]() - a && (e.y -= a), n.tipPosition.x = e.x, n.tipPosition.y = e.y
                        }, e.showTooltip = function (t, i) {
                            var o = n.getDomElement(), a = n.stringFormat(n.tooltipOptions.content, t);
                            "" !== a && (o.html(a), e.setTooltipPosition({
                                x: i.pageX,
                                y: i.pageY
                            }), o.css({
                                left: n.tipPosition.x + n.tooltipOptions.shifts.x,
                                top: n.tipPosition.y + n.tooltipOptions.shifts.y
                            }).show(), "function" == typeof n.tooltipOptions.onHover && n.tooltipOptions.onHover(t, o))
                        }, e.hideTooltip = function () {
                            n.getDomElement().hide().html("")
                        }
                    }, i.prototype.getDomElement = function () {
                        var e = t("." + this.tooltipOptions.cssClass);
                        return 0 === e.length && (e = t("<div />").addClass(this.tooltipOptions.cssClass), e.appendTo("body").hide().css({position: "absolute"}), this.tooltipOptions.defaultTheme && e.css({
                            background: "#fff",
                            "z-index": "1040",
                            padding: "0.4em 0.6em",
                            "border-radius": "0.5em",
                            "font-size": "0.8em",
                            border: "1px solid #111",
                            display: "none",
                            "white-space": "nowrap"
                        })), e
                    }, i.prototype.stringFormat = function (t, e) {
                        var i, o, n, a, r = /%p\.{0,1}(\d{0,})/, s = /%s/, l = /%c/, c = /%lx/, u = /%ly/, h = /%x\.{0,1}(\d{0,})/, p = /%y\.{0,1}(\d{0,})/, f = "%x", d = "%y", m = "%ct";
                        if ("undefined" != typeof e.series.threshold ? (i = e.datapoint[0], o = e.datapoint[1], n = e.datapoint[2]) : "undefined" != typeof e.series.lines && e.series.lines.steps ? (i = e.series.datapoints.points[2 * e.dataIndex], o = e.series.datapoints.points[2 * e.dataIndex + 1], n = "") : (i = e.series.data[e.dataIndex][0], o = e.series.data[e.dataIndex][1], n = e.series.data[e.dataIndex][2]), null === e.series.label && e.series.originSeries && (e.series.label = e.series.originSeries.label), "function" == typeof t && (t = t(e.series.label, i, o, e)), "boolean" == typeof t && !t)return "";
                        if ("undefined" != typeof e.series.percent ? a = e.series.percent : "undefined" != typeof e.series.percents && (a = e.series.percents[e.dataIndex]), "number" == typeof a && (t = this.adjustValPrecision(r, t, a)), t = "undefined" != typeof e.series.label ? t.replace(s, e.series.label) : t.replace(s, ""), t = "undefined" != typeof e.series.color ? t.replace(l, e.series.color) : t.replace(l, ""), t = this.hasAxisLabel("xaxis", e) ? t.replace(c, e.series.xaxis.options.axisLabel) : t.replace(c, ""), t = this.hasAxisLabel("yaxis", e) ? t.replace(u, e.series.yaxis.options.axisLabel) : t.replace(u, ""), this.isTimeMode("xaxis", e) && this.isXDateFormat(e) && (t = t.replace(h, this.timestampToDate(i, this.tooltipOptions.xDateFormat, e.series.xaxis.options))), this.isTimeMode("yaxis", e) && this.isYDateFormat(e) && (t = t.replace(p, this.timestampToDate(o, this.tooltipOptions.yDateFormat, e.series.yaxis.options))), "number" == typeof i && (t = this.adjustValPrecision(h, t, i)), "number" == typeof o && (t = this.adjustValPrecision(p, t, o)), "undefined" != typeof e.series.xaxis.ticks) {
                            var g;
                            g = this.hasRotatedXAxisTicks(e) ? "rotatedTicks" : "ticks";
                            var x = e.dataIndex + e.seriesIndex;
                            for (var v in e.series.xaxis[g])if (e.series.xaxis[g].hasOwnProperty(x) && !this.isTimeMode("xaxis", e)) {
                                var b = this.isCategoriesMode("xaxis", e) ? e.series.xaxis[g][x].label : e.series.xaxis[g][x].v;
                                b === i && (t = t.replace(h, e.series.xaxis[g][x].label))
                            }
                        }
                        if ("undefined" != typeof e.series.yaxis.ticks)for (var v in e.series.yaxis.ticks)if (e.series.yaxis.ticks.hasOwnProperty(v)) {
                            var y = this.isCategoriesMode("yaxis", e) ? e.series.yaxis.ticks[v].label : e.series.yaxis.ticks[v].v;
                            y === o && (t = t.replace(p, e.series.yaxis.ticks[v].label))
                        }
                        return "undefined" != typeof e.series.xaxis.tickFormatter && (t = t.replace(f, e.series.xaxis.tickFormatter(i, e.series.xaxis).replace(/\$/g, "$$"))), "undefined" != typeof e.series.yaxis.tickFormatter && (t = t.replace(d, e.series.yaxis.tickFormatter(o, e.series.yaxis).replace(/\$/g, "$$"))), n && (t = t.replace(m, n)), t
                    }, i.prototype.isTimeMode = function (t, e) {
                        return "undefined" != typeof e.series[t].options.mode && "time" === e.series[t].options.mode
                    }, i.prototype.isXDateFormat = function () {
                        return "undefined" != typeof this.tooltipOptions.xDateFormat && null !== this.tooltipOptions.xDateFormat
                    }, i.prototype.isYDateFormat = function () {
                        return "undefined" != typeof this.tooltipOptions.yDateFormat && null !== this.tooltipOptions.yDateFormat
                    }, i.prototype.isCategoriesMode = function (t, e) {
                        return "undefined" != typeof e.series[t].options.mode && "categories" === e.series[t].options.mode
                    }, i.prototype.timestampToDate = function (e, i, o) {
                        var n = t.plot.dateGenerator(e, o);
                        return t.plot.formatDate(n, i, this.tooltipOptions.monthNames, this.tooltipOptions.dayNames)
                    }, i.prototype.adjustValPrecision = function (t, e, i) {
                        var o, n = e.match(t);
                        return null !== n && "" !== RegExp.$1 && (o = RegExp.$1, i = i.toFixed(o), e = e.replace(t, i)), e
                    }, i.prototype.hasAxisLabel = function (e, i) {
                        return -1 !== t.inArray(this.plotPlugins, "axisLabels") && "undefined" != typeof i.series[e].options.axisLabel && i.series[e].options.axisLabel.length > 0
                    }, i.prototype.hasRotatedXAxisTicks = function (e) {
                        return -1 !== t.inArray(this.plotPlugins, "tickRotor") && "undefined" != typeof e.series.xaxis.rotatedTicks
                    };
                    var o = function (t) {
                        new i(t)
                    };
                    t.plot.plugins.push({init: o, options: e, name: "tooltip", version: "0.8.5"})
                }(t)
            })
        }.apply(t, arguments)
    })
}(this), function (t) {
    define("flot-navigate", ["flot"], function () {
        return function () {
            require(["jquery", "flot"], function (t) {
                !function (t) {
                    function e(n) {
                        var c, u = this, h = n.data || {};
                        if (h.elem) u = n.dragTarget = h.elem, n.dragProxy = l.proxy || u, n.cursorOffsetX = h.pageX - h.left, n.cursorOffsetY = h.pageY - h.top, n.offsetX = n.pageX - n.cursorOffsetX, n.offsetY = n.pageY - n.cursorOffsetY; else if (l.dragging || h.which > 0 && n.which != h.which || t(n.target).is(h.not))return;
                        switch (n.type) {
                            case"mousedown":
                                return t.extend(h, t(u).offset(), {
                                    elem: u,
                                    target: n.target,
                                    pageX: n.pageX,
                                    pageY: n.pageY
                                }), r.add(document, "mousemove mouseup", e, h), a(u, !1), l.dragging = null, !1;
                            case!l.dragging && "mousemove":
                                if (o(n.pageX - h.pageX) + o(n.pageY - h.pageY) < h.distance)break;
                                n.target = h.target, c = i(n, "dragstart", u), c !== !1 && (l.dragging = u, l.proxy = n.dragProxy = t(c || u)[0]);
                            case"mousemove":
                                if (l.dragging) {
                                    if (c = i(n, "drag", u), s.drop && (s.drop.allowed = c !== !1, s.drop.handler(n)), c !== !1)break;
                                    n.type = "mouseup"
                                }
                            case"mouseup":
                                r.remove(document, "mousemove mouseup", e), l.dragging && (s.drop && s.drop.handler(n), i(n, "dragend", u)), a(u, !0), l.dragging = l.proxy = h.elem = !1
                        }
                        return !0
                    }

                    function i(e, i, o) {
                        e.type = i;
                        var n = t.event.dispatch.call(o, e);
                        return n === !1 ? !1 : n || e.result
                    }

                    function o(t) {
                        return Math.pow(t, 2)
                    }

                    function n() {
                        return l.dragging === !1
                    }

                    function a(t, e) {
                        t && (t.unselectable = e ? "off" : "on", t.onselectstart = function () {
                            return e
                        }, t.style && (t.style.MozUserSelect = e ? "" : "none"))
                    }

                    t.fn.drag = function (t, e, i) {
                        return e && this.bind("dragstart", t), i && this.bind("dragend", i), t ? this.bind("drag", e ? e : t) : this.trigger("drag")
                    };
                    var r = t.event, s = r.special, l = s.drag = {
                        not: ":input",
                        distance: 0,
                        which: 1,
                        dragging: !1,
                        setup: function (i) {
                            i = t.extend({
                                distance: l.distance,
                                which: l.which,
                                not: l.not
                            }, i || {}), i.distance = o(i.distance), r.add(this, "mousedown", e, i), this.attachEvent && this.attachEvent("ondragstart", n)
                        },
                        teardown: function () {
                            r.remove(this, "mousedown", e), this === l.dragging && (l.dragging = l.proxy = !1), a(this, !0), this.detachEvent && this.detachEvent("ondragstart", n)
                        }
                    };
                    s.dragstart = s.dragend = {
                        setup: function () {
                        }, teardown: function () {
                        }
                    }
                }(t), function (t) {
                    function e(e) {
                        var i = e || window.event, o = [].slice.call(arguments, 1), n = 0, a = 0, r = 0, e = t.event.fix(i);
                        return e.type = "mousewheel", i.wheelDelta && (n = i.wheelDelta / 120), i.detail && (n = -i.detail / 3), r = n, void 0 !== i.axis && i.axis === i.HORIZONTAL_AXIS && (r = 0, a = -1 * n), void 0 !== i.wheelDeltaY && (r = i.wheelDeltaY / 120), void 0 !== i.wheelDeltaX && (a = -1 * i.wheelDeltaX / 120), o.unshift(e, n, a, r), (t.event.dispatch || t.event.handle).apply(this, o)
                    }

                    var i = ["DOMMouseScroll", "mousewheel"];
                    if (t.event.fixHooks)for (var o = i.length; o;)t.event.fixHooks[i[--o]] = t.event.mouseHooks;
                    t.event.special.mousewheel = {
                        setup: function () {
                            if (this.addEventListener)for (var t = i.length; t;)this.addEventListener(i[--t], e, !1); else this.onmousewheel = e
                        }, teardown: function () {
                            if (this.removeEventListener)for (var t = i.length; t;)this.removeEventListener(i[--t], e, !1); else this.onmousewheel = null
                        }
                    }, t.fn.extend({
                        mousewheel: function (t) {
                            return t ? this.bind("mousewheel", t) : this.trigger("mousewheel")
                        }, unmousewheel: function (t) {
                            return this.unbind("mousewheel", t)
                        }
                    })
                }(t), function (t) {
                    function e(e) {
                        function i(t, i) {
                            var o = e.offset();
                            o.left = t.pageX - o.left, o.top = t.pageY - o.top, i ? e.zoomOut({center: o}) : e.zoom({center: o})
                        }

                        function o(t, e) {
                            return t.preventDefault(), i(t, 0 > e), !1
                        }

                        function n(t) {
                            if (1 != t.which)return !1;
                            var i = e.getPlaceholder().css("cursor");
                            i && (c = i), e.getPlaceholder().css("cursor", e.getOptions().pan.cursor), u = t.pageX, h = t.pageY
                        }

                        function a(t) {
                            var i = e.getOptions().pan.frameRate;
                            !p && i && (p = setTimeout(function () {
                                e.pan({left: u - t.pageX, top: h - t.pageY}), u = t.pageX, h = t.pageY, p = null
                            }, 1 / i * 1e3))
                        }

                        function r(t) {
                            p && (clearTimeout(p), p = null), e.getPlaceholder().css("cursor", c), e.pan({
                                left: u - t.pageX,
                                top: h - t.pageY
                            })
                        }

                        function s(t, e) {
                            var s = t.getOptions();
                            s.zoom.interactive && (e[s.zoom.trigger](i), e.mousewheel(o)), s.pan.interactive && (e.bind("dragstart", {distance: 10}, n), e.bind("drag", a), e.bind("dragend", r))
                        }

                        function l(t, e) {
                            e.unbind(t.getOptions().zoom.trigger, i), e.unbind("mousewheel", o), e.unbind("dragstart", n), e.unbind("drag", a), e.unbind("dragend", r), p && clearTimeout(p)
                        }

                        var c = "default", u = 0, h = 0, p = null;
                        e.zoomOut = function (t) {
                            t || (t = {}), t.amount || (t.amount = e.getOptions().zoom.amount), t.amount = 1 / t.amount, e.zoom(t)
                        }, e.zoom = function (i) {
                            i || (i = {});
                            var o = i.center, n = i.amount || e.getOptions().zoom.amount, a = e.width(), r = e.height();
                            o || (o = {left: a / 2, top: r / 2});
                            var s = o.left / a, l = o.top / r, c = {
                                x: {
                                    min: o.left - s * a / n,
                                    max: o.left + (1 - s) * a / n
                                }, y: {min: o.top - l * r / n, max: o.top + (1 - l) * r / n}
                            };
                            t.each(e.getAxes(), function (t, e) {
                                var i = e.options, o = c[e.direction].min, a = c[e.direction].max, r = i.zoomRange, s = i.panRange;
                                if (r !== !1) {
                                    if (o = e.c2p(o), a = e.c2p(a), o > a) {
                                        var l = o;
                                        o = a, a = l
                                    }
                                    s && (null != s[0] && o < s[0] && (o = s[0]), null != s[1] && a > s[1] && (a = s[1]));
                                    var u = a - o;
                                    r && (null != r[0] && u < r[0] && n > 1 || null != r[1] && u > r[1] && 1 > n) || (i.min = o, i.max = a)
                                }
                            }), e.setupGrid(), e.draw(), i.preventEvent || e.getPlaceholder().trigger("plotzoom", [e, i])
                        }, e.pan = function (i) {
                            var o = {x: +i.left, y: +i.top};
                            isNaN(o.x) && (o.x = 0), isNaN(o.y) && (o.y = 0), t.each(e.getAxes(), function (t, e) {
                                var i, n, a = e.options, r = o[e.direction];
                                i = e.c2p(e.p2c(e.min) + r), n = e.c2p(e.p2c(e.max) + r);
                                var s = a.panRange;
                                s !== !1 && (s && (null != s[0] && s[0] > i && (r = s[0] - i, i += r, n += r), null != s[1] && s[1] < n && (r = s[1] - n, i += r, n += r)), a.min = i, a.max = n)
                            }), e.setupGrid(), e.draw(), i.preventEvent || e.getPlaceholder().trigger("plotpan", [e, i])
                        }, e.hooks.bindEvents.push(s), e.hooks.shutdown.push(l)
                    }

                    var i = {
                        xaxis: {zoomRange: null, panRange: null},
                        zoom: {interactive: !1, trigger: "dblclick", amount: 1.5},
                        pan: {interactive: !1, cursor: "move", frameRate: 20}
                    };
                    t.plot.plugins.push({init: e, options: i, name: "navigate", version: "1.3"})
                }(t)
            })
        }.apply(t, arguments)
    })
}(this), define("widget/timechart", ["sos-data-access", "locale-date", "widget-common", "jquery", "flot", "flot-resize", "flot-time", "flot-tooltip", "flot-navigate"], function (t, e, i, o) {
    "use strict";
    var n = ['<div class="timechart widget">', '<h3 style="width:100%"></h3>', '<div class="graph" style="height:75%; width: 100%; max-height: 380px;"></div>', '<div id="legend" style="display: inline-block; float: right; margin-right: 15px; margin-left: 50px; margin-top: 10px"></div>', '<div><span class="footnote"></span></div>', "</div>"].join("");
    return {
        inputs: i.inputs.concat(["features", "properties", "time_start", "time_end", "title"]),
        optional_inputs: i.optional_inputs,
        preferredSizes: [{w: 650, h: 530}],
        init: function (a, r, s) {
            function l(t) {
                var i = {};
                for (var n in t) {
                    var a = t[n], r = a.property + " (" + a.feature + ")";
                    i[r] || (i[r] = {data: [], label: r}), i[r].data.push([a.time.getTime(), a.value])
                }
                var s = function (t, e) {
                    return e[0] - t[0]
                }, l = [];
                for (var u in i)i[u].data.sort(s), l.push(i[u]);
                var h = {
                    xaxis: {mode: "time", timezone: e.utc() ? "UTC" : "browser"},
                    yaxis: {zoomRange: !1, panRange: !1},
                    grid: {hoverable: !0},
                    legend: {container: "#legend"},
                    series: {lines: {show: !0}, points: {show: !0}},
                    tooltip: !0,
                    tooltipOpts: {content: "[%x] %s: %y.2 " + t[0].uom},
                    zoom: {interactive: !0},
                    pan: {interactive: !0}
                };
                o.plot(c, l, h)
            }

            r.innerHTML = n, r.querySelector("h3").innerHTML = a.title;
            var c = r.querySelector(".graph");
            i.init(a, r);
            var u = t(a, l, s);
            u.read()
        }
    }
});