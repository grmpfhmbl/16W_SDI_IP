/** vim: et:ts=4:sw=4:sts=4
 * @license RequireJS 2.1.18 Copyright (c) 2010-2015, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/jrburke/requirejs for details
 */

/**
 * @license RequireJS text 2.0.14 Copyright (c) 2010-2014, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/requirejs/text for details
 */

/* This work is licensed under Creative Commons GNU LGPL License.

 License: http://creativecommons.org/licenses/LGPL/2.1/
 Version: 0.9
 Author:  Stefan Goessner/2006
 See:     http://goessner.net/download/prj/jsonxml/
 */

var requirejs, require, define;
!function (global) {
    function isFunction(e) {
        return "[object Function]" === ostring.call(e)
    }

    function isArray(e) {
        return "[object Array]" === ostring.call(e)
    }

    function each(e, n) {
        if (e) {
            var t;
            for (t = 0; t < e.length && (!e[t] || !n(e[t], t, e)); t += 1);
        }
    }

    function eachReverse(e, n) {
        if (e) {
            var t;
            for (t = e.length - 1; t > -1 && (!e[t] || !n(e[t], t, e)); t -= 1);
        }
    }

    function hasProp(e, n) {
        return hasOwn.call(e, n)
    }

    function getOwn(e, n) {
        return hasProp(e, n) && e[n]
    }

    function eachProp(e, n) {
        var t;
        for (t in e)if (hasProp(e, t) && n(e[t], t))break
    }

    function mixin(e, n, t, r) {
        return n && eachProp(n, function (n, i) {
            (t || !hasProp(e, i)) && (!r || "object" != typeof n || !n || isArray(n) || isFunction(n) || n instanceof RegExp ? e[i] = n : (e[i] || (e[i] = {}), mixin(e[i], n, t, r)))
        }), e
    }

    function bind(e, n) {
        return function () {
            return n.apply(e, arguments)
        }
    }

    function scripts() {
        return document.getElementsByTagName("script")
    }

    function defaultOnError(e) {
        throw e
    }

    function getGlobal(e) {
        if (!e)return e;
        var n = global;
        return each(e.split("."), function (e) {
            n = n[e]
        }), n
    }

    function makeError(e, n, t, r) {
        var i = new Error(n + "\nhttp://requirejs.org/docs/errors.html#" + e);
        return i.requireType = e, i.requireModules = r, t && (i.originalError = t), i
    }

    function newContext(e) {
        function n(e) {
            var n, t;
            for (n = 0; n < e.length; n++)if (t = e[n], "." === t) e.splice(n, 1), n -= 1; else if (".." === t) {
                if (0 === n || 1 === n && ".." === e[2] || ".." === e[n - 1])continue;
                n > 0 && (e.splice(n - 1, 2), n -= 2)
            }
        }

        function t(e, t, r) {
            var i, a, o, s, c, u, l, d, p, f, m, h, g = t && t.split("/"), v = w.map, b = v && v["*"];
            if (e && (e = e.split("/"), l = e.length - 1, w.nodeIdCompat && jsSuffixRegExp.test(e[l]) && (e[l] = e[l].replace(jsSuffixRegExp, "")), "." === e[0].charAt(0) && g && (h = g.slice(0, g.length - 1), e = h.concat(e)), n(e), e = e.join("/")), r && v && (g || b)) {
                o = e.split("/");
                e:for (s = o.length; s > 0; s -= 1) {
                    if (u = o.slice(0, s).join("/"), g)for (c = g.length; c > 0; c -= 1)if (a = getOwn(v, g.slice(0, c).join("/")), a && (a = getOwn(a, u))) {
                        d = a, p = s;
                        break e
                    }
                    !f && b && getOwn(b, u) && (f = getOwn(b, u), m = s)
                }
                !d && f && (d = f, p = m), d && (o.splice(0, p, d), e = o.join("/"))
            }
            return i = getOwn(w.pkgs, e), i ? i : e
        }

        function r(e) {
            isBrowser && each(scripts(), function (n) {
                return n.getAttribute("data-requiremodule") === e && n.getAttribute("data-requirecontext") === x.contextName ? (n.parentNode.removeChild(n), !0) : void 0
            })
        }

        function i(e) {
            var n = getOwn(w.paths, e);
            return n && isArray(n) && n.length > 1 ? (n.shift(), x.require.undef(e), x.makeRequire(null, {skipMap: !0})([e]), !0) : void 0
        }

        function a(e) {
            var n, t = e ? e.indexOf("!") : -1;
            return t > -1 && (n = e.substring(0, t), e = e.substring(t + 1, e.length)), [n, e]
        }

        function o(e, n, r, i) {
            var o, s, c, u, l = null, d = n ? n.name : null, p = e, f = !0, m = "";
            return e || (f = !1, e = "_@r" + (k += 1)), u = a(e), l = u[0], e = u[1], l && (l = t(l, d, i), s = getOwn(M, l)), e && (l ? m = s && s.normalize ? s.normalize(e, function (e) {
                        return t(e, d, i)
                    }) : -1 === e.indexOf("!") ? t(e, d, i) : e : (m = t(e, d, i), u = a(m), l = u[0], m = u[1], r = !0, o = x.nameToUrl(m))), c = !l || s || r ? "" : "_unnormalized" + (R += 1), {
                prefix: l,
                name: m,
                parentMap: n,
                unnormalized: !!c,
                url: o,
                originalName: p,
                isDefine: f,
                id: (l ? l + "!" + m : m) + c
            }
        }

        function s(e) {
            var n = e.id, t = getOwn(E, n);
            return t || (t = E[n] = new x.Module(e)), t
        }

        function c(e, n, t) {
            var r = e.id, i = getOwn(E, r);
            !hasProp(M, r) || i && !i.defineEmitComplete ? (i = s(e), i.error && "error" === n ? t(i.error) : i.on(n, t)) : "defined" === n && t(M[r])
        }

        function u(e, n) {
            var t = e.requireModules, r = !1;
            n ? n(e) : (each(t, function (n) {
                    var t = getOwn(E, n);
                    t && (t.error = e, t.events.error && (r = !0, t.emit("error", e)))
                }), r || req.onError(e))
        }

        function l() {
            globalDefQueue.length && (apsp.apply(T, [T.length, 0].concat(globalDefQueue)), globalDefQueue = [])
        }

        function d(e) {
            delete E[e], delete O[e]
        }

        function p(e, n, t) {
            var r = e.map.id;
            e.error ? e.emit("error", e.error) : (n[r] = !0, each(e.depMaps, function (r, i) {
                    var a = r.id, o = getOwn(E, a);
                    !o || e.depMatched[i] || t[a] || (getOwn(n, a) ? (e.defineDep(i, M[a]), e.check()) : p(o, n, t))
                }), t[r] = !0)
        }

        function f() {
            var e, n, t = 1e3 * w.waitSeconds, a = t && x.startTime + t < (new Date).getTime(), o = [], s = [], c = !1, l = !0;
            if (!b) {
                if (b = !0, eachProp(O, function (e) {
                        var t = e.map, u = t.id;
                        if (e.enabled && (t.isDefine || s.push(e), !e.error))if (!e.inited && a) i(u) ? (n = !0, c = !0) : (o.push(u), r(u)); else if (!e.inited && e.fetched && t.isDefine && (c = !0, !t.prefix))return l = !1
                    }), a && o.length)return e = makeError("timeout", "Load timeout for modules: " + o, null, o), e.contextName = x.contextName, u(e);
                l && each(s, function (e) {
                    p(e, {}, {})
                }), a && !n || !c || !isBrowser && !isWebWorker || S || (S = setTimeout(function () {
                    S = 0, f()
                }, 50)), b = !1
            }
        }

        function m(e) {
            hasProp(M, e[0]) || s(o(e[0], null, !0)).init(e[1], e[2])
        }

        function h(e, n, t, r) {
            e.detachEvent && !isOpera ? r && e.detachEvent(r, n) : e.removeEventListener(t, n, !1)
        }

        function g(e) {
            var n = e.currentTarget || e.srcElement;
            return h(n, x.onScriptLoad, "load", "onreadystatechange"), h(n, x.onScriptError, "error"), {
                node: n,
                id: n && n.getAttribute("data-requiremodule")
            }
        }

        function v() {
            var e;
            for (l(); T.length;) {
                if (e = T.shift(), null === e[0])return u(makeError("mismatch", "Mismatched anonymous define() module: " + e[e.length - 1]));
                m(e)
            }
        }

        var b, y, x, q, S, w = {
            waitSeconds: 7,
            baseUrl: "./",
            paths: {},
            bundles: {},
            pkgs: {},
            shim: {},
            config: {}
        }, E = {}, O = {}, j = {}, T = [], M = {}, C = {}, P = {}, k = 1, R = 1;
        return q = {
            require: function (e) {
                return e.require ? e.require : e.require = x.makeRequire(e.map)
            }, exports: function (e) {
                return e.usingExports = !0, e.map.isDefine ? e.exports ? M[e.map.id] = e.exports : e.exports = M[e.map.id] = {} : void 0
            }, module: function (e) {
                return e.module ? e.module : e.module = {
                        id: e.map.id, uri: e.map.url, config: function () {
                            return getOwn(w.config, e.map.id) || {}
                        }, exports: e.exports || (e.exports = {})
                    }
            }
        }, y = function (e) {
            this.events = getOwn(j, e.id) || {}, this.map = e, this.shim = getOwn(w.shim, e.id), this.depExports = [], this.depMaps = [], this.depMatched = [], this.pluginMaps = {}, this.depCount = 0
        }, y.prototype = {
            init: function (e, n, t, r) {
                r = r || {}, this.inited || (this.factory = n, t ? this.on("error", t) : this.events.error && (t = bind(this, function (e) {
                        this.emit("error", e)
                    })), this.depMaps = e && e.slice(0), this.errback = t, this.inited = !0, this.ignore = r.ignore, r.enabled || this.enabled ? this.enable() : this.check())
            }, defineDep: function (e, n) {
                this.depMatched[e] || (this.depMatched[e] = !0, this.depCount -= 1, this.depExports[e] = n)
            }, fetch: function () {
                if (!this.fetched) {
                    this.fetched = !0, x.startTime = (new Date).getTime();
                    var e = this.map;
                    return this.shim ? void x.makeRequire(this.map, {enableBuildCallback: !0})(this.shim.deps || [], bind(this, function () {
                            return e.prefix ? this.callPlugin() : this.load()
                        })) : e.prefix ? this.callPlugin() : this.load()
                }
            }, load: function () {
                var e = this.map.url;
                C[e] || (C[e] = !0, x.load(this.map.id, e))
            }, check: function () {
                if (this.enabled && !this.enabling) {
                    var e, n, t = this.map.id, r = this.depExports, i = this.exports, a = this.factory;
                    if (this.inited) {
                        if (this.error) this.emit("error", this.error); else if (!this.defining) {
                            if (this.defining = !0, this.depCount < 1 && !this.defined) {
                                if (isFunction(a)) {
                                    if (this.events.error && this.map.isDefine || req.onError !== defaultOnError)try {
                                        i = x.execCb(t, a, r, i)
                                    } catch (o) {
                                        e = o
                                    } else i = x.execCb(t, a, r, i);
                                    if (this.map.isDefine && void 0 === i && (n = this.module, n ? i = n.exports : this.usingExports && (i = this.exports)), e)return e.requireMap = this.map, e.requireModules = this.map.isDefine ? [this.map.id] : null, e.requireType = this.map.isDefine ? "define" : "require", u(this.error = e)
                                } else i = a;
                                this.exports = i, this.map.isDefine && !this.ignore && (M[t] = i, req.onResourceLoad && req.onResourceLoad(x, this.map, this.depMaps)), d(t), this.defined = !0
                            }
                            this.defining = !1, this.defined && !this.defineEmitted && (this.defineEmitted = !0, this.emit("defined", this.exports), this.defineEmitComplete = !0)
                        }
                    } else this.fetch()
                }
            }, callPlugin: function () {
                var e = this.map, n = e.id, r = o(e.prefix);
                this.depMaps.push(r), c(r, "defined", bind(this, function (r) {
                    var i, a, l, p = getOwn(P, this.map.id), f = this.map.name, m = this.map.parentMap ? this.map.parentMap.name : null, h = x.makeRequire(e.parentMap, {enableBuildCallback: !0});
                    return this.map.unnormalized ? (r.normalize && (f = r.normalize(f, function (e) {
                                return t(e, m, !0)
                            }) || ""), a = o(e.prefix + "!" + f, this.map.parentMap), c(a, "defined", bind(this, function (e) {
                            this.init([], function () {
                                return e
                            }, null, {enabled: !0, ignore: !0})
                        })), l = getOwn(E, a.id), void(l && (this.depMaps.push(a), this.events.error && l.on("error", bind(this, function (e) {
                            this.emit("error", e)
                        })), l.enable()))) : p ? (this.map.url = x.nameToUrl(p), void this.load()) : (i = bind(this, function (e) {
                                this.init([], function () {
                                    return e
                                }, null, {enabled: !0})
                            }), i.error = bind(this, function (e) {
                                this.inited = !0, this.error = e, e.requireModules = [n], eachProp(E, function (e) {
                                    0 === e.map.id.indexOf(n + "_unnormalized") && d(e.map.id)
                                }), u(e)
                            }), i.fromText = bind(this, function (t, r) {
                                var a = e.name, c = o(a), l = useInteractive;
                                r && (t = r), l && (useInteractive = !1), s(c), hasProp(w.config, n) && (w.config[a] = w.config[n]);
                                try {
                                    req.exec(t)
                                } catch (d) {
                                    return u(makeError("fromtexteval", "fromText eval for " + n + " failed: " + d, d, [n]))
                                }
                                l && (useInteractive = !0), this.depMaps.push(c), x.completeLoad(a), h([a], i)
                            }), void r.load(e.name, h, i, w))
                })), x.enable(r, this), this.pluginMaps[r.id] = r
            }, enable: function () {
                O[this.map.id] = this, this.enabled = !0, this.enabling = !0, each(this.depMaps, bind(this, function (e, n) {
                    var t, r, i;
                    if ("string" == typeof e) {
                        if (e = o(e, this.map.isDefine ? this.map : this.map.parentMap, !1, !this.skipMap), this.depMaps[n] = e, i = getOwn(q, e.id))return void(this.depExports[n] = i(this));
                        this.depCount += 1, c(e, "defined", bind(this, function (e) {
                            this.undefed || (this.defineDep(n, e), this.check())
                        })), this.errback ? c(e, "error", bind(this, this.errback)) : this.events.error && c(e, "error", bind(this, function (e) {
                                this.emit("error", e)
                            }))
                    }
                    t = e.id, r = E[t], hasProp(q, t) || !r || r.enabled || x.enable(e, this)
                })), eachProp(this.pluginMaps, bind(this, function (e) {
                    var n = getOwn(E, e.id);
                    n && !n.enabled && x.enable(e, this)
                })), this.enabling = !1, this.check()
            }, on: function (e, n) {
                var t = this.events[e];
                t || (t = this.events[e] = []), t.push(n)
            }, emit: function (e, n) {
                each(this.events[e], function (e) {
                    e(n)
                }), "error" === e && delete this.events[e]
            }
        }, x = {
            config: w,
            contextName: e,
            registry: E,
            defined: M,
            urlFetched: C,
            defQueue: T,
            Module: y,
            makeModuleMap: o,
            nextTick: req.nextTick,
            onError: u,
            configure: function (e) {
                e.baseUrl && "/" !== e.baseUrl.charAt(e.baseUrl.length - 1) && (e.baseUrl += "/");
                var n = w.shim, t = {paths: !0, bundles: !0, config: !0, map: !0};
                eachProp(e, function (e, n) {
                    t[n] ? (w[n] || (w[n] = {}), mixin(w[n], e, !0, !0)) : w[n] = e
                }), e.bundles && eachProp(e.bundles, function (e, n) {
                    each(e, function (e) {
                        e !== n && (P[e] = n)
                    })
                }), e.shim && (eachProp(e.shim, function (e, t) {
                    isArray(e) && (e = {deps: e}), !e.exports && !e.init || e.exportsFn || (e.exportsFn = x.makeShimExports(e)), n[t] = e
                }), w.shim = n), e.packages && each(e.packages, function (e) {
                    var n, t;
                    e = "string" == typeof e ? {name: e} : e, t = e.name, n = e.location, n && (w.paths[t] = e.location), w.pkgs[t] = e.name + "/" + (e.main || "main").replace(currDirRegExp, "").replace(jsSuffixRegExp, "")
                }), eachProp(E, function (e, n) {
                    e.inited || e.map.unnormalized || (e.map = o(n, null, !0))
                }), (e.deps || e.callback) && x.require(e.deps || [], e.callback)
            },
            makeShimExports: function (e) {
                function n() {
                    var n;
                    return e.init && (n = e.init.apply(global, arguments)), n || e.exports && getGlobal(e.exports)
                }

                return n
            },
            makeRequire: function (n, i) {
                function a(t, r, c) {
                    var l, d, p;
                    return i.enableBuildCallback && r && isFunction(r) && (r.__requireJsBuild = !0), "string" == typeof t ? isFunction(r) ? u(makeError("requireargs", "Invalid require call"), c) : n && hasProp(q, t) ? q[t](E[n.id]) : req.get ? req.get(x, t, n, a) : (d = o(t, n, !1, !0), l = d.id, hasProp(M, l) ? M[l] : u(makeError("notloaded", 'Module name "' + l + '" has not been loaded yet for context: ' + e + (n ? "" : ". Use require([])")))) : (v(), x.nextTick(function () {
                            v(), p = s(o(null, n)), p.skipMap = i.skipMap, p.init(t, r, c, {enabled: !0}), f()
                        }), a)
                }

                return i = i || {}, mixin(a, {
                    isBrowser: isBrowser, toUrl: function (e) {
                        var r, i = e.lastIndexOf("."), a = e.split("/")[0], o = "." === a || ".." === a;
                        return -1 !== i && (!o || i > 1) && (r = e.substring(i, e.length), e = e.substring(0, i)), x.nameToUrl(t(e, n && n.id, !0), r, !0)
                    }, defined: function (e) {
                        return hasProp(M, o(e, n, !1, !0).id)
                    }, specified: function (e) {
                        return e = o(e, n, !1, !0).id, hasProp(M, e) || hasProp(E, e)
                    }
                }), n || (a.undef = function (e) {
                    l();
                    var t = o(e, n, !0), i = getOwn(E, e);
                    i.undefed = !0, r(e), delete M[e], delete C[t.url], delete j[e], eachReverse(T, function (n, t) {
                        n[0] === e && T.splice(t, 1)
                    }), i && (i.events.defined && (j[e] = i.events), d(e))
                }), a
            },
            enable: function (e) {
                var n = getOwn(E, e.id);
                n && s(e).enable()
            },
            completeLoad: function (e) {
                var n, t, r, a = getOwn(w.shim, e) || {}, o = a.exports;
                for (l(); T.length;) {
                    if (t = T.shift(), null === t[0]) {
                        if (t[0] = e, n)break;
                        n = !0
                    } else t[0] === e && (n = !0);
                    m(t)
                }
                if (r = getOwn(E, e), !n && !hasProp(M, e) && r && !r.inited) {
                    if (!(!w.enforceDefine || o && getGlobal(o)))return i(e) ? void 0 : u(makeError("nodefine", "No define call for " + e, null, [e]));
                    m([e, a.deps || [], a.exportsFn])
                }
                f()
            },
            nameToUrl: function (e, n, t) {
                var r, i, a, o, s, c, u, l = getOwn(w.pkgs, e);
                if (l && (e = l), u = getOwn(P, e))return x.nameToUrl(u, n, t);
                if (req.jsExtRegExp.test(e)) s = e + (n || ""); else {
                    for (r = w.paths, i = e.split("/"), a = i.length; a > 0; a -= 1)if (o = i.slice(0, a).join("/"), c = getOwn(r, o)) {
                        isArray(c) && (c = c[0]), i.splice(0, a, c);
                        break
                    }
                    s = i.join("/"), s += n || (/^data\:|\?/.test(s) || t ? "" : ".js"), s = ("/" === s.charAt(0) || s.match(/^[\w\+\.\-]+:/) ? "" : w.baseUrl) + s
                }
                return w.urlArgs ? s + ((-1 === s.indexOf("?") ? "?" : "&") + w.urlArgs) : s
            },
            load: function (e, n) {
                req.load(x, e, n)
            },
            execCb: function (e, n, t, r) {
                return n.apply(r, t)
            },
            onScriptLoad: function (e) {
                if ("load" === e.type || readyRegExp.test((e.currentTarget || e.srcElement).readyState)) {
                    interactiveScript = null;
                    var n = g(e);
                    x.completeLoad(n.id)
                }
            },
            onScriptError: function (e) {
                var n = g(e);
                return i(n.id) ? void 0 : u(makeError("scripterror", "Script error for: " + n.id, e, [n.id]))
            }
        }, x.require = x.makeRequire(), x
    }

    function getInteractiveScript() {
        return interactiveScript && "interactive" === interactiveScript.readyState ? interactiveScript : (eachReverse(scripts(), function (e) {
                return "interactive" === e.readyState ? interactiveScript = e : void 0
            }), interactiveScript)
    }

    var req, s, head, baseElement, dataMain, src, interactiveScript, currentlyAddingScript, mainScript, subPath, version = "2.1.18", commentRegExp = /(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/gm, cjsRequireRegExp = /[^.]\s*require\s*\(\s*["']([^'"\s]+)["']\s*\)/g, jsSuffixRegExp = /\.js$/, currDirRegExp = /^\.\//, op = Object.prototype, ostring = op.toString, hasOwn = op.hasOwnProperty, ap = Array.prototype, apsp = ap.splice, isBrowser = !("undefined" == typeof window || "undefined" == typeof navigator || !window.document), isWebWorker = !isBrowser && "undefined" != typeof importScripts, readyRegExp = isBrowser && "PLAYSTATION 3" === navigator.platform ? /^complete$/ : /^(complete|loaded)$/, defContextName = "_", isOpera = "undefined" != typeof opera && "[object Opera]" === opera.toString(), contexts = {}, cfg = {}, globalDefQueue = [], useInteractive = !1;
    if ("undefined" == typeof define) {
        if ("undefined" != typeof requirejs) {
            if (isFunction(requirejs))return;
            cfg = requirejs, requirejs = void 0
        }
        "undefined" == typeof require || isFunction(require) || (cfg = require, require = void 0), req = requirejs = function (e, n, t, r) {
            var i, a, o = defContextName;
            return isArray(e) || "string" == typeof e || (a = e, isArray(n) ? (e = n, n = t, t = r) : e = []), a && a.context && (o = a.context), i = getOwn(contexts, o), i || (i = contexts[o] = req.s.newContext(o)), a && i.configure(a), i.require(e, n, t)
        }, req.config = function (e) {
            return req(e)
        }, req.nextTick = "undefined" != typeof setTimeout ? function (e) {
                setTimeout(e, 4)
            } : function (e) {
                e()
            }, require || (require = req), req.version = version, req.jsExtRegExp = /^\/|:|\?|\.js$/, req.isBrowser = isBrowser, s = req.s = {
            contexts: contexts,
            newContext: newContext
        }, req({}), each(["toUrl", "undef", "defined", "specified"], function (e) {
            req[e] = function () {
                var n = contexts[defContextName];
                return n.require[e].apply(n, arguments)
            }
        }), isBrowser && (head = s.head = document.getElementsByTagName("head")[0], baseElement = document.getElementsByTagName("base")[0], baseElement && (head = s.head = baseElement.parentNode)), req.onError = defaultOnError, req.createNode = function (e) {
            var n = e.xhtml ? document.createElementNS("http://www.w3.org/1999/xhtml", "html:script") : document.createElement("script");
            return n.type = e.scriptType || "text/javascript", n.charset = "utf-8", n.async = !0, n
        }, req.load = function (e, n, t) {
            var r, i = e && e.config || {};
            if (isBrowser)return r = req.createNode(i, n, t), r.setAttribute("data-requirecontext", e.contextName), r.setAttribute("data-requiremodule", n), !r.attachEvent || r.attachEvent.toString && r.attachEvent.toString().indexOf("[native code") < 0 || isOpera ? (r.addEventListener("load", e.onScriptLoad, !1), r.addEventListener("error", e.onScriptError, !1)) : (useInteractive = !0, r.attachEvent("onreadystatechange", e.onScriptLoad)), r.src = t, currentlyAddingScript = r, baseElement ? head.insertBefore(r, baseElement) : head.appendChild(r), currentlyAddingScript = null, r;
            if (isWebWorker)try {
                importScripts(t), e.completeLoad(n)
            } catch (a) {
                e.onError(makeError("importscripts", "importScripts failed for " + n + " at " + t, a, [n]))
            }
        }, isBrowser && !cfg.skipDataMain && eachReverse(scripts(), function (e) {
            return head || (head = e.parentNode), dataMain = e.getAttribute("data-main"), dataMain ? (mainScript = dataMain, cfg.baseUrl || (src = mainScript.split("/"), mainScript = src.pop(), subPath = src.length ? src.join("/") + "/" : "./", cfg.baseUrl = subPath), mainScript = mainScript.replace(jsSuffixRegExp, ""), req.jsExtRegExp.test(mainScript) && (mainScript = dataMain), cfg.deps = cfg.deps ? cfg.deps.concat(mainScript) : [mainScript], !0) : void 0
        }), define = function (e, n, t) {
            var r, i;
            "string" != typeof e && (t = n, n = e, e = null), isArray(n) || (t = n, n = null), !n && isFunction(t) && (n = [], t.length && (t.toString().replace(commentRegExp, "").replace(cjsRequireRegExp, function (e, t) {
                n.push(t)
            }), n = (1 === t.length ? ["require"] : ["require", "exports", "module"]).concat(n))), useInteractive && (r = currentlyAddingScript || getInteractiveScript(), r && (e || (e = r.getAttribute("data-requiremodule")), i = contexts[r.getAttribute("data-requirecontext")])), (i ? i.defQueue : globalDefQueue).push([e, n, t])
        }, define.amd = {jQuery: !0}, req.exec = function (text) {
            return eval(text)
        }, req(cfg)
    }
}(this), define("requireLib", function () {
}), define("text", ["module"], function (e) {
    "use strict";
    var n, t, r, i, a, o = ["Msxml2.XMLHTTP", "Microsoft.XMLHTTP", "Msxml2.XMLHTTP.4.0"], s = /^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im, c = /<body[^>]*>\s*([\s\S]+)\s*<\/body>/im, u = "undefined" != typeof location && location.href, l = u && location.protocol && location.protocol.replace(/\:/, ""), d = u && location.hostname, p = u && (location.port || void 0), f = {}, m = e.config && e.config() || {};
    return n = {
        version: "2.0.14", strip: function (e) {
            if (e) {
                e = e.replace(s, "");
                var n = e.match(c);
                n && (e = n[1])
            } else e = "";
            return e
        }, jsEscape: function (e) {
            return e.replace(/(['\\])/g, "\\$1").replace(/[\f]/g, "\\f").replace(/[\b]/g, "\\b").replace(/[\n]/g, "\\n").replace(/[\t]/g, "\\t").replace(/[\r]/g, "\\r").replace(/[\u2028]/g, "\\u2028").replace(/[\u2029]/g, "\\u2029")
        }, createXhr: m.createXhr || function () {
            var e, n, t;
            if ("undefined" != typeof XMLHttpRequest)return new XMLHttpRequest;
            if ("undefined" != typeof ActiveXObject)for (n = 0; 3 > n; n += 1) {
                t = o[n];
                try {
                    e = new ActiveXObject(t)
                } catch (r) {
                }
                if (e) {
                    o = [t];
                    break
                }
            }
            return e
        }, parseName: function (e) {
            var n, t, r, i = !1, a = e.lastIndexOf("."), o = 0 === e.indexOf("./") || 0 === e.indexOf("../");
            return -1 !== a && (!o || a > 1) ? (n = e.substring(0, a), t = e.substring(a + 1)) : n = e, r = t || n, a = r.indexOf("!"), -1 !== a && (i = "strip" === r.substring(a + 1), r = r.substring(0, a), t ? t = r : n = r), {
                moduleName: n,
                ext: t,
                strip: i
            }
        }, xdRegExp: /^((\w+)\:)?\/\/([^\/\\]+)/, useXhr: function (e, t, r, i) {
            var a, o, s, c = n.xdRegExp.exec(e);
            return c ? (a = c[2], o = c[3], o = o.split(":"), s = o[1], o = o[0], !(a && a !== t || o && o.toLowerCase() !== r.toLowerCase() || (s || o) && s !== i)) : !0
        }, finishLoad: function (e, t, r, i) {
            r = t ? n.strip(r) : r, m.isBuild && (f[e] = r), i(r)
        }, load: function (e, t, r, i) {
            if (i && i.isBuild && !i.inlineText)return void r();
            m.isBuild = i && i.isBuild;
            var a = n.parseName(e), o = a.moduleName + (a.ext ? "." + a.ext : ""), s = t.toUrl(o), c = m.useXhr || n.useXhr;
            return 0 === s.indexOf("empty:") ? void r() : void(!u || c(s, l, d, p) ? n.get(s, function (t) {
                        n.finishLoad(e, a.strip, t, r)
                    }, function (e) {
                        r.error && r.error(e)
                    }) : t([o], function (e) {
                        n.finishLoad(a.moduleName + "." + a.ext, a.strip, e, r)
                    }))
        }, write: function (e, t, r) {
            if (f.hasOwnProperty(t)) {
                var i = n.jsEscape(f[t]);
                r.asModule(e + "!" + t, "define(function () { return '" + i + "';});\n")
            }
        }, writeFile: function (e, t, r, i, a) {
            var o = n.parseName(t), s = o.ext ? "." + o.ext : "", c = o.moduleName + s, u = r.toUrl(o.moduleName + s) + ".js";
            n.load(c, r, function () {
                var t = function (e) {
                    return i(u, e)
                };
                t.asModule = function (e, n) {
                    return i.asModule(e, u, n)
                }, n.write(e, c, t, a)
            }, a)
        }
    }, "node" === m.env || !m.env && "undefined" != typeof process && process.versions && process.versions.node && !process.versions["node-webkit"] && !process.versions["atom-shell"] ? (t = require.nodeRequire("fs"), n.get = function (e, n, r) {
            try {
                var i = t.readFileSync(e, "utf8");
                "﻿" === i[0] && (i = i.substring(1)), n(i)
            } catch (a) {
                r && r(a)
            }
        }) : "xhr" === m.env || !m.env && n.createXhr() ? n.get = function (e, t, r, i) {
                var a, o = n.createXhr();
                if (o.open("GET", e, !0), i)for (a in i)i.hasOwnProperty(a) && o.setRequestHeader(a.toLowerCase(), i[a]);
                m.onXhr && m.onXhr(o, e), o.onreadystatechange = function () {
                    var n, i;
                    4 === o.readyState && (n = o.status || 0, n > 399 && 600 > n ? (i = new Error(e + " HTTP status: " + n), i.xhr = o, r && r(i)) : t(o.responseText), m.onXhrComplete && m.onXhrComplete(o, e))
                }, o.send(null)
            } : "rhino" === m.env || !m.env && "undefined" != typeof Packages && "undefined" != typeof java ? n.get = function (e, n) {
                    var t, r, i = "utf-8", a = new java.io.File(e), o = java.lang.System.getProperty("line.separator"), s = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(a), i)), c = "";
                    try {
                        for (t = new java.lang.StringBuffer, r = s.readLine(), r && r.length() && 65279 === r.charAt(0) && (r = r.substring(1)), null !== r && t.append(r); null !== (r = s.readLine());)t.append(o), t.append(r);
                        c = String(t.toString())
                    } finally {
                        s.close()
                    }
                    n(c)
                } : ("xpconnect" === m.env || !m.env && "undefined" != typeof Components && Components.classes && Components.interfaces) && (r = Components.classes, i = Components.interfaces, Components.utils["import"]("resource://gre/modules/FileUtils.jsm"), a = "@mozilla.org/windows-registry-key;1" in r, n.get = function (e, n) {
                    var t, o, s, c = {};
                    a && (e = e.replace(/\//g, "\\")), s = new FileUtils.File(e);
                    try {
                        t = r["@mozilla.org/network/file-input-stream;1"].createInstance(i.nsIFileInputStream), t.init(s, 1, 0, !1), o = r["@mozilla.org/intl/converter-input-stream;1"].createInstance(i.nsIConverterInputStream), o.init(t, "utf-8", t.available(), i.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER), o.readString(t.available(), c), o.close(), t.close(), n(c.value)
                    } catch (u) {
                        throw new Error((s && s.path || "") + ": " + u)
                    }
                }), n
}), define("text!translations.json", [], function () {
    return '{\n  "langs": {\n    "en": "English",\n    "es": "Español",\n    "ca": "Català"\n  },\n  "(no date)": {\n    "es": "(sin fecha)",\n    "ca": "(sense data)"\n  },\n  "No widget name specified": {\n    "es": "No se ha especificado ningún nombre de widget",\n    "ca": "Cal especificar un nom de widget"\n  },\n  "Widget \'{name}\' cannot be found": {\n    "es": "No se ha encontrado el widget de nombre \'{name}\'",\n    "ca": "No s\'ha trobat cap widget anomenat \'{name}\'"\n  },\n  "The \'{name}\' widget is missing some mandatory parameters: ": {\n    "es": "Faltan algunos parámetros obligatorios para el widget \'{name}\': ",\n    "ca": "Cal afegir els següents paràmetres obligatoris al widget \'{name}\': "\n  },\n  "Loading...": {\n    "es": "Cargando...",\n    "ca": "Carregant..."\n  },\n  "deg": {\n    "es": "º",\n    "ca": "º"\n  },\n  "Request time": {\n    "es": "Petición el",\n    "ca": "Petició el"\n  },\n  "Response time": {\n    "es": "Respuesta el",\n    "ca": "Resposta el"\n  },\n  "(no data)": {\n    "es": "(sin datos)",\n    "ca": "(sense dades)"\n  },\n  "Result time": {\n    "es": "Resultado del",\n    "ca": "Resultat del"\n  },\n  "Cel": {\n    "es": "ºC",\n    "ca": "ºC"\n  },\n  "Results": {\n    "es": "Resultados",\n    "ca": "Resultats"\n  },\n  "Time": {\n    "es": "Hora",\n    "ca": "Hora"\n  },\n  "Value": {\n    "es": "Valor",\n    "ca": "Valor"\n  },\n  "Unit": {\n    "es": "Unidad",\n    "ca": "Unitat"\n  },\n  "{name} Widget Configuration": {\n    "es": "Configuración de {name}",\n    "ca": "Configuració de {name}"\n  },\n  "Mandatory inputs": {\n    "es": "Parámetros obligatorios",\n    "ca": "Paràmetres obligatoris"\n  },\n  "Compass": {\n    "es": "Rumbo",\n    "ca": "Rumb"\n  },\n  "Gauge": {\n    "es": "Manómetro",\n    "ca": "Manòmetre"\n  },\n  "Jqgrid": {\n    "es": "Tabla JQuery",\n    "ca": "Taula JQuery"\n  },\n  "Map": {\n    "es": "Mapa",\n    "ca": "Mapa"\n  },\n  "Panel": {\n    "es": "Panel",\n    "ca": "Panell"\n  },\n  "Progressbar": {\n    "es": "Barra",\n    "ca": "Barra"\n  },\n  "Table": {\n    "es": "Tabla",\n    "ca": "Taula"\n  },\n  "Thermometer": {\n    "es": "Termómetro",\n    "ca": "Termòmetre"\n  },\n  "Timechart": {\n    "es": "Serie tiempo",\n    "ca": "Sèrie temps"\n  },\n  "Windrose": {\n    "es": "Rosa vientos",\n    "ca": "Rosa vents"\n  },\n  "Select a Service...": {\n    "es": "Seleccione un servicio...",\n    "ca": "Sel·leccioneu un servei..."\n  },\n  "Select an Offering...": {\n    "es": "Seleccione un offering...",\n    "ca": "Sel·leccioneu un offering..."\n  },\n  "(multiselect)": {\n    "es": "(selección múltiple)",\n    "ca": "(selecció múltiple)"\n  },\n  "Service": {\n    "es": "Servicio",\n    "ca": "Servei"\n  },\n  "Property": {\n    "es": "Propiedad",\n    "ca": "Propietat"\n  },\n  "Properties": {\n    "es": "Propiedades",\n    "ca": "Propietats"\n  },\n  "Refresh Interval": {\n    "es": "Intervalo de refresco",\n    "ca": "Intèrval de refresc"\n  },\n  "Time Range": {\n    "es": "Rango de tiempo",\n    "ca": "Rang de temps"\n  },\n  "MMM D, YYYY H:mm": {\n    "es": "D MMM YYYY H:mm",\n    "ca": "D MMM YYYY H:mm"\n  },\n  "Custom Range": {\n    "es": "Rango predefinido",\n    "ca": "Rang predefinit"\n  },\n  "Today": {\n    "es": "Hoy",\n    "ca": "Avui"\n  },\n  "Last hour": {\n    "es": "La última hora",\n    "ca": "La última hora"\n  },\n  "Last {n} hours": {\n    "es": "Las últimas {n} horas",\n    "ca": "Les últimes {n} hores"\n  },\n  "From": {\n    "es": "De",\n    "ca": "De"\n  },\n  "To": {\n    "es": "A",\n    "ca": "A"\n  },\n  "Apply": {\n    "es": "Aplicar",\n    "ca": "Aplica"\n  },\n  "Cancel": {\n    "es": "Cancelar",\n    "ca": "Cancel·la"\n  },\n  "W": {\n    "es": "S",\n    "ca": "S"\n  },\n  "Optional inputs": {\n    "es": "Parámetros opcionales",\n    "ca": "Paràmetres opcionals"\n  },\n  "Title": {\n    "es": "Título",\n    "ca": "Títol"\n  },\n  "Footnote": {\n    "es": "Nota al pie",\n    "ca": "Nota al peu"\n  },\n  "Custom Css Url": {\n    "es": "URL del CSS personalizado",\n    "ca": "URL del CSS personalitzat"\n  },\n  "Max Initial Zoom": {\n    "es": "Zoom Inicial Máximo",\n    "ca": "Zoom Inicial Màxim"\n  },\n  "Base Layer": {\n    "es": "Mapa de Base",\n    "ca": "Mapa de Base"\n  },\n  "Widget dimensions": {\n    "es": "Tamaño del widget",\n    "ca": "Mides del widget"\n  },\n  "Initial Size": {\n    "es": "Tamaño inicial",\n    "ca": "Mida inicial"\n  },\n  "Create Widget": {\n    "es": "Crear Widget",\n    "ca": "Crear Widget"\n  },\n  "{name} Configuration Parameters": {\n    "es": "Parámetros de {name}",\n    "ca": "Paràmetres de {name}"\n  },\n  "jqGrid Example": {\n    "es": "Ejemplo de tabla JQuery",\n    "ca": "Exemple de taula JQuery"\n  },\n  "Last observations": {\n    "es": "Observaciones más recientes",\n    "ca": "Darreres observacions"\n  },\n  "Data Table - last 3 hours": {\n    "es": "Tabla de datos - últimas 3 horas",\n    "ca": "Taula de dades - darreres 3 hores"\n  },\n  "Sirena Windrose": {\n    "es": "Rosa de los Vientos Sirena",\n    "ca": "Rosa dels Vents Sirena"\n  },\n  "Last 3 hours of wind observations": {\n    "es": "Últimas 3 horas de observaciones del viento",\n    "ca": "Darreres 3 hores d\'observacions del vent"\n  },\n  "Code": {\n    "es": "Código",\n    "ca": "Codi"\n  },\n  "Embed": {\n    "es": "Incrustar",\n    "ca": "Incrusta"\n  },\n  "Link": {\n    "es": "Enlazar",\n    "ca": "Enllaça"\n  },\n  "Mandatory": {\n    "es": "Obligatorios",\n    "ca": "Obligatoris"\n  },\n  "Optional": {\n    "es": "Opcionales",\n    "ca": "Opcionals"\n  },\n  "Suggested Sizes": {\n    "es": "Tamaños recomendados",\n    "ca": "Mides recomanades"\n  },\n  "A sample footnote for {name} widget": {\n    "es": "Nota al pie de ejemplo en el widget {name}",\n    "ca": "Nota al peu d\'exemple al widget {name}"\n  }\n}\n'
}), define("i18n", ["text!translations.json"], function (e) {
    "use strict";
    function n(e) {
        o = e, console.debug("Language set to " + o)
    }

    function t(e, n) {
        for (var t in n)e = e.replace(new RegExp("{" + t + "}", "g"), n[t]);
        return e
    }

    function r(e, n) {
        return i.hasOwnProperty(e) && i[e].hasOwnProperty(o) && (e = i[e][o]), t(e, n)
    }

    var i = JSON.parse(e), a = {};
    location.search.substr(1).split("&").forEach(function (e) {
        var n = e.split("=");
        a[n[0]] = n[1]
    });
    var o;
    return n(a.hasOwnProperty("lang") ? a.lang : "en"), {
        langs: function () {
            return i.langs
        }, getLang: function () {
            return o
        }, setLang: n, t: r, addTranslations: function (e) {
            Object.keys(e).forEach(function (n) {
                i.hasOwnProperty(n) ? console.warn("Skipping duplicate entry '" + n + "' in translation bundle.") : i[n] = e[n]
            })
        }, translateDocTree: function (e) {
            e || (e = document);
            for (var n = document.createTreeWalker(e, NodeFilter.SHOW_TEXT, null, !1); n.nextNode();) {
                var t = n.currentNode;
                /\S/.test(t.nodeValue) && (t.nodeValue = r(t.nodeValue))
            }
        }
    }
}), define("css", {
    load: function (e, n, t) {
        function r(e) {
            var n = document.getElementsByTagName("head")[0], r = document.createElement("link");
            r.href = e, r.rel = "stylesheet", r.type = "text/css", r.onload = function () {
                t()
            }, n.appendChild(r)
        }

        "undefined" == typeof document ? t() : r(n.toUrl(e))
    }
}), define("SensorWidget", ["i18n", "css!SensorWidgets.css"], function (e) {
    "use strict";
    var n = {}, t = function (e) {
        return function () {
            return "SensorWidgetTarget-" + ++e
        }
    }(0);
    return function (r, i, a) {
        function o(e, n, t) {
            var r = "";
            n && (r = "[" + n + "] "), t && t.request && (r += t.request + ": "), e && (r += e), a.innerHTML = '<div class="text-danger">' + r + "</div>"
        }

        function s(n, t, r) {
            var i = [];
            for (var a in t) {
                var s = t[a];
                r.hasOwnProperty(s) || i.push(s)
            }
            return i.length && o(e.t("The '{name}' widget is missing some mandatory parameters: ", {name: n}) + i.join(", ")), !i.length
        }

        return a || (a = document.body), r && i ? (a.id || (a.id = t()), i.service || (i.service = "/52n-sos/sos/json"), require(["widget/" + r], function (e) {
                a.innerHTML = "", n.hasOwnProperty(a.id) && n[a.id] && n[a.id].hasOwnProperty("destroy") && (console.debug("Destroying previous widget on ElementId=" + a.id), n[a.id].destroy(), delete n[a.id]), s(r, e.inputs, i) && (console.debug("Creating new " + r + " widget on ElementId=" + a.id), n[a.id] = e.init(i, a, o))
            }, function () {
                o(e.t("Widget '{name}' cannot be found", {name: r}))
            })) : r || o(e.t("No widget name specified")), {
            name: r, config: i, renderTo: a, inspect: function (e) {
                require(["widget/" + r], function (n) {
                    e.call(this, n.inputs, n.optional_inputs, n.preferredSizes)
                })
            }, url: function () {
                function n(e) {
                    var n = [];
                    return e.replace(/^(\.\.?(\/|$))+/, "").replace(/\/(\.(\/|$))+/g, "/").replace(/\/\.\.$/, "/../").replace(/\/?[^\/]*/g, function (e) {
                        "/.." === e ? n.pop() : n.push(e)
                    }), n.join("").replace(/^\//, "/" === e.charAt(0) ? "/" : "")
                }

                var t = n(require.toUrl("../widget/")) + "?";
                return t += "name=" + encodeURIComponent(r) + "&", t += Object.keys(i).map(function (e) {
                    var n = i[e];
                    return "object" == typeof i[e] && (n = JSON.stringify(i[e])), e + "=" + encodeURIComponent(n)
                }).join("&"), t += "&lang=" + e.getLang()
            }, iframe: function (e, n) {
                return e = e ? e : "100%", n = n ? n : "100%", '<iframe src="' + this.url() + '" width="' + e + '" height="' + n + '" frameBorder="0"></iframe>'
            }, javascript: function () {
                return "SensorWidget('" + r + "', " + JSON.stringify(i, null, 3) + ", document.getElementById('" + r + "-container'));"
            }
        }
    }
}), require.config({
    waitSeconds: 30,
    baseUrl: "../js",
    paths: {
        text: "../lib/requirejs-text/text",
        bootstrap: "../lib/bootstrap/bootstrap.amd",
        daterangepicker: "../lib/bootstrap-daterangepicker/daterangepicker",
        flot: "../lib/flot/jquery.flot.amd",
        "flot-navigate": "../lib/flot/jquery.flot.navigate.amd",
        "flot-resize": "../lib/flot/jquery.flot.resize.amd",
        "flot-time": "../lib/flot/jquery.flot.time.amd",
        "flot-tooltip": "../lib/flot.tooltip/jquery.flot.tooltip.amd",
        highcharts: "../lib/highcharts/highcharts",
        "highcharts-more": "../lib/highcharts/highcharts-more",
        highlight: "../lib/highlightjs/highlight.pack",
        jquery: "../lib/jquery/jquery",
        "jquery-ui": "../lib/jquery-ui/jquery-ui",
        jqgrid: "../lib/jqgrid/jquery.jqGrid.amd",
        "jqgrid-locale-en": "../lib/jqgrid/grid.locale-en.amd",
        leaflet: "../lib/leaflet/leaflet",
        "leaflet-label": "../lib/Leaflet.label/leaflet.label",
        moment: "../lib/moment/moment",
        "moment-es": "../lib/moment/locale/es",
        "moment-ca": "../lib/moment/locale/ca"
    },
    map: {"*": {jquery: "jquery-noconflict"}, "jquery-noconflict": {jquery: "jquery"}},
    shim: {
        daterangepicker: {deps: ["bootstrap", "moment-es", "moment-ca", "jquery", "css!../lib/bootstrap-daterangepicker/daterangepicker-bs3.css"]},
        flot: {deps: ["jquery"]},
        "flot-navigate": {deps: ["flot"]},
        "flot-resize": {deps: ["flot"]},
        "flot-time": {deps: ["flot"]},
        "flot-tooltip": {deps: ["flot"]},
        highcharts: {exports: "Highcharts"},
        "highcharts-more": {deps: ["highcharts"]},
        highlight: {deps: ["css!../lib/highlightjs/color-brewer.css"]},
        "jquery-ui": {deps: ["jquery", "css!../css/jquery-ui.css"]},
        jqgrid: {deps: ["jquery-ui", "jqgrid-locale-en", "css!../lib/jqgrid/ui.jqgrid.css"]},
        leaflet: {deps: ["css!../lib/leaflet/leaflet.css"]},
        "leaflet-label": {deps: ["leaflet", "css!../lib/Leaflet.label/leaflet.label.css"]},
        "moment-es": {deps: ["moment"]},
        "moment-ca": {deps: ["moment"]}
    }
}), function (e) {
    var n;
    if (document.currentScript) n = document.currentScript; else {
        var t = document.getElementsByTagName("script");
        n = t[t.length - 1]
    }
    var r = n.src.replace(/[^\/]*$/, "");
    console.debug("Sensor Widgets' Base URL is: " + r), e.config({baseUrl: r})
}(requirejs), window.SensorWidget = function () {
    var e = arguments;
    require(["SensorWidget"], function (n) {
        n.apply(this, e)
    })
}, define("main", function () {
}), define("XML", [], function () {
    "use strict";
    return {
        read: function (e, n) {
            var t = {
                at: n ? "" : "@", toObj: function (e) {
                    var r = {};
                    if (1 == e.nodeType) {
                        if (e.attributes.length)for (var i = 0; i < e.attributes.length; i++) {
                            var a = e.attributes[i].name, o = e.attributes[i].value, s = 0 === a.lastIndexOf("xmlns:", 0);
                            n && s || (r[t.at + a] = (o || "").toString())
                        }
                        if (e.firstChild) {
                            for (var c = 0, u = 0, l = !1, d = e.firstChild; d; d = d.nextSibling)1 == d.nodeType ? l = !0 : 3 == d.nodeType && d.nodeValue.match(/[^ \f\n\r\t\v]/) ? c++ : 4 == d.nodeType && u++;
                            if (l)if (2 > c && 2 > u)for (t.removeWhite(e), d = e.firstChild; d; d = d.nextSibling)3 == d.nodeType ? r["#text"] = t.escape(d.nodeValue) : 4 == d.nodeType ? r["#cdata"] = t.escape(d.nodeValue) : r[d.nodeName] ? r[d.nodeName] instanceof Array ? r[d.nodeName][r[d.nodeName].length] = t.toObj(d) : r[d.nodeName] = [r[d.nodeName], t.toObj(d)] : r[d.nodeName] = t.toObj(d); else e.attributes.length ? r["#text"] = t.escape(t.innerXml(e)) : r = t.escape(t.innerXml(e)); else if (c) e.attributes.length ? r["#text"] = t.escape(t.innerXml(e)) : r = t.escape(t.innerXml(e)); else if (u)if (u > 1) r = t.escape(t.innerXml(e)); else for (d = e.firstChild; d; d = d.nextSibling)r["#cdata"] = t.escape(d.nodeValue)
                        }
                        e.attributes.length || e.firstChild || (r = null)
                    } else if (9 == e.nodeType) r = t.toObj(e.documentElement); else {
                        if (8 == e.nodeType)return e.data;
                        console.error("unhandled node type: " + e.nodeType)
                    }
                    return r
                }, innerXml: function (e) {
                    var n = "";
                    if ("innerHTML" in e) n = e.innerHTML; else for (var t = function (e) {
                        var n = "";
                        if (1 == e.nodeType) {
                            n += "<" + e.nodeName;
                            for (var r = 0; r < e.attributes.length; r++) {
                                var i = e.attributes[r].name, a = e.attributes[r].value || "";
                                n += " " + i + '="' + a.toString() + '"'
                            }
                            if (e.firstChild) {
                                n += ">";
                                for (var o = e.firstChild; o; o = o.nextSibling)n += t(o);
                                n += "</" + e.nodeName + ">"
                            } else n += "/>"
                        } else 3 == e.nodeType ? n += e.nodeValue : 4 == e.nodeType && (n += "<![CDATA[" + e.nodeValue + "]]>");
                        return n
                    }, r = e.firstChild; r; r = r.nextSibling)n += t(r);
                    return n
                }, escape: function (e) {
                    return e.replace(/[\\]/g, "\\\\").replace(/[\"]/g, '\\"').replace(/[\n]/g, "\\n").replace(/[\r]/g, "\\r")
                }, removeWhite: function (e) {
                    e.normalize();
                    for (var n = e.firstChild; n;)if (3 == n.nodeType)if (n.nodeValue.match(/[^ \f\n\r\t\v]/)) n = n.nextSibling; else {
                        var r = n.nextSibling;
                        e.removeChild(n), n = r
                    } else 1 == n.nodeType ? (t.removeWhite(n), n = n.nextSibling) : n = n.nextSibling;
                    return e
                }
            };
            n && (e = e.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>")), e = (new DOMParser).parseFromString(e, "text/xml"), 9 == e.nodeType && (e = e.documentElement);
            var r = {};
            return r[e.nodeName] = t.toObj(t.removeWhite(e)), r
        }, write: function (e) {
            var n = function (e, t, r) {
                var i = "";
                if (e instanceof Array)for (var a = 0, o = e.length; o > a; a++)i += r + n(e[a], t, r + "	") + "\n"; else if ("object" == typeof e) {
                    var s = !1;
                    i += r + "<" + t;
                    for (var c in e)"@" == c.charAt(0) ? i += " " + c.substr(1) + '="' + e[c].toString() + '"' : s = !0;
                    if (i += s ? ">" : "/>", s) {
                        for (c in e)"#text" == c ? i += e[c] : "#cdata" == c ? i += "<![CDATA[" + e[c] + "]]>" : "@" != c.charAt(0) && (i += n(e[c], c, r + "	"));
                        i += ("\n" == i.charAt(i.length - 1) ? r : "") + "</" + t + ">"
                    }
                } else i += r + "<" + t + ">" + e.toString() + "</" + t + ">";
                return i
            }, t = "";
            for (var r in e)t += n(e[r], r, "");
            return t
        }
    }
}), define("SOS", ["XML"], function (e) {
    "use strict";
    var n = {
        _url: null, setUrl: function (e) {
            this._url = e
        }, getCapabilities: function (e, n) {
            var t = {request: "GetCapabilities", sections: ["Contents"]};
            this._send(t, function (n) {
                e(n.contents)
            }, n)
        }, describeSensor: function (n, t, r) {
            var i = {
                request: "DescribeSensor",
                procedure: n,
                procedureDescriptionFormat: "http://www.opengis.net/sensorML/1.0.1"
            };
            this._send(i, function (n) {
                var r = n.procedureDescription.hasOwnProperty("description") ? n.procedureDescription.description : n.procedureDescription, i = e.read(r, !0);
                t(i.SensorML.member)
            }, r)
        }, getFeatureOfInterest: function (e, n, t) {
            var r = {request: "GetFeatureOfInterest", procedure: e};
            this._send(r, function (e) {
                n(e.featureOfInterest)
            }, t)
        }, getDataAvailability: function (e, n, t, r, i) {
            var a = {request: "GetDataAvailability"};
            e && (a.procedure = e), n && n.length && (a.featureOfInterest = n), t && t.length && (a.observedProperty = t), this._send(a, function (e) {
                r(e.dataAvailability)
            }, i)
        }, getObservation: function (e, n, t, r, i, a) {
            var o = {request: "GetObservation"};
            if (e && (o.offering = e), n && n.length && (o.featureOfInterest = n), t && t.length && (o.observedProperty = t), r) {
                var s;
                s = r.length && 2 == r.length ? "during" : "equals";
                var c = {};
                c[s] = {ref: "om:resultTime", value: r}, o.temporalFilter = [c]
            }
            this._send(o, function (e) {
                i(e.observations)
            }, a)
        }, _send: function (e, n, t) {
            e.service = "SOS", e.version = "2.0.0";
            var r = new XMLHttpRequest;
            r.onreadystatechange = function () {
                if (4 == r.readyState) {
                    var i = r.responseText;
                    try {
                        i = JSON.parse(i)
                    } catch (a) {
                    }
                    if (200 == r.status) n.call(this, i); else {
                        var a = {status: r.statusText, url: this._url, request: e, response: i};
                        console.log(a), t && t.call(this, a.status, a.url, a.request, a.response)
                    }
                }
            }.bind(this), r.open("POST", this._url, !0), r.setRequestHeader("Content-Type", "application/json"), r.setRequestHeader("Accept", "application/json"), r.send(JSON.stringify(e))
        }
    };
    return n
}), define("sos-data-access", ["SOS"], function (e) {
    "use strict";
    var n = {}, t = {}, r = {};
    return function (i, a, o) {
        function s() {
            var n = i.offering, t = i.feature ? [i.feature] : c(i.features) ? i.features : i.features ? JSON.parse(i.features) : void 0, r = i.property ? [i.property] : c(i.properties) ? i.properties : i.properties ? JSON.parse(i.properties) : void 0, a = i.time_start && i.time_end ? [i.time_start, i.time_end] : "latest";
            e.getObservation(n, t, r, a, u, o)
        }

        function c(e) {
            return "[object Array]" === Object.prototype.toString.call(e)
        }

        function u(e) {
            function n(n, r) {
                t.push({
                    time: new Date(r.resultTime),
                    value: r.result.hasOwnProperty("value") ? r.result.value : r.result,
                    feature: r.featureOfInterest.name.value,
                    property: n,
                    uom: r.result.hasOwnProperty("uom") ? r.result.uom : "(N/A)"
                }), t.length == e.length && a(t)
            }

            e.length || a([]);
            var t = [];
            for (var r in e) {
                var i = e[r];
                l(i.procedure, i.observableProperty, n, i)
            }
        }

        function l(i, a, s, c) {
            n[i] ? s(n[i][a], c) : (r[i] || (r[i] = []), r[i].push({
                    callback: s,
                    id: a,
                    context: c
                }), t[i] || (t[i] = !0, e.describeSensor(i, function (e) {
                    var t = e.hasOwnProperty("ProcessModel") ? e.ProcessModel.outputs.OutputList.output : e.System.outputs.OutputList.output;
                    t = t instanceof Array ? t : [t];
                    var a = ["Quantity", "Count", "Boolean", "Category", "Text", "ObservableProperty"], o = [];
                    for (var s in t) {
                        var c = t[s];
                        for (var u in a) {
                            var l = a[u];
                            c.hasOwnProperty(l) && (c.id = c[l].definition)
                        }
                        o[c.id] = c.name
                    }
                    for (n[i] = o; r[i].length;) {
                        var d = r[i].shift();
                        d.callback.call(void 0, n[i][d.id], d.context)
                    }
                }, o)))
        }

        return e.setUrl(i.service), {read: s}
    }
}), define("widget-common", [], function () {
    "use strict";
    function e(e) {
        var n = document.createElement("link");
        n.setAttribute("rel", "stylesheet"), n.setAttribute("type", "text/css"), n.setAttribute("href", e), "undefined" != typeof n && document.getElementsByTagName("head")[0].appendChild(n)
    }

    return {
        inputs: ["service", "offering"], optional_inputs: ["footnote", "custom_css_url"], init: function (n, t) {
            void 0 !== n.custom_css_url && e(n.custom_css_url), void 0 !== n.footnote && t.querySelector(".footnote") && (t.querySelector(".footnote").innerHTML = n.footnote)
        }
    }
}), define("locale-date", ["i18n"], function (e) {
    "use strict";
    var n = {utc: !1, locale: navigator.language || navigator.browserLanguage};
    return {
        display: function (t) {
            return t ? n.utc ? t.toLocaleString(n.locale, {timeZone: "UTC"}) + " UTC" : t.toLocaleString(n.locale) : e.t("(no date)")
        }, locale: function (e) {
            return e && (n.locale = e), n.locale
        }, utc: function (e) {
            return "undefined" != typeof e && (n.utc = e), n.utc
        }
    }
}), define("SensorWidgets", function () {
});